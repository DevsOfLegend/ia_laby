======
README
======

:Info: See <https://bitbucket.org/DevsOfLegend/ia_laby> for repo.
:Author: NUEL Guillaume (ImmortalPC) and DECK Léa
:Date: $Date: 2012-11-30 $
:Revision: $Revision: 153 $
:Description: "Jeu" de labyrinthe où une fourmi doit atteindre la sortie en jouant le moins de coups possibles. Illustration de l'utilisation des algo: "Le meilleur d'abord" (BestFirst), A* (ASearch).


**Description**
---------------
"Jeu" de labyrinthe où une fourmi doit atteindre la sortie en jouant le moins de coups possibles. Illustration de l'utilisation des algo: "Le meilleur d'abord" (BestFirst), A* (ASearch).

| Ce programme comprend les fonctionnalité suivantes:

- Générateur de labyrinthe. (Dimension et nombre ( / position ) de sortie variables)
- Nombre de sortie variable 0 -> inf
- Système d'affichage multiple: SWING et mode console
- Implémentation des méthodes de déplacement: "Le meilleur d'abord" (BestFirst), A* (ASearch)


**Compilation**
---------------
Java **7** (JDK 7) est nécéssaire pour la compilation !

>>> javac src/*.java -d bin


**Format** **du** **code**
--------------------------
- Le code est en UTF-8
- Le code est indenté avec des tabulations réel (\\t)
- Le code est prévus pour être affiché avec une taille de 4 pour les tab (\\t)
- Les fins de lignes sont de type LF (\\n)
- IDE utilisé: Eclipse


**Normes**
----------
- Les commentaires sont au format doxygen


**Utilisation**
---------------
Le programme dispose de 2 modes d'affichages:

- Mode console ( UNIQUEMENT SOUS **UNIX** )
- Mode graphique ( TOUT OS )

Pour lancer le programme en mode **CONSOLE**

>>> java Main_EntryPoint isConsoleMode=true

Pour lancer le programme en mode **graphique** :

>>> java Main_EntryPoint isConsoleMode=false

| Il y a différent paramètres qui peuvent être appliqués.
| Pour plus d'informations :

>>> java Main_EntryPoint help

.. Attention::
	Pour avoir un mode console correct, veuillez utiliser un système **UNIX**.
	La console a un mode d'affichage qui est inconfortable a lire (voir illisible) sous Windows.


**Documentation**
-----------------
Le code a entièrement documenté avec doxygen.
Pour générer la documentation:

>>> ./doxygen


**IDE** **RST**
---------------
RST créé grace au visualisateur: http://rst.ninjs.org/
