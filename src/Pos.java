import java.util.ArrayList;

/***************************************************************************//*!
 * Permet de gérer une position 2D (X,Y)
 */
public class Pos
{
	private int m_x=0;//!< Position X
	private int m_y=0;//!< Position Y

	/***********************************************************************//*!
	 * Contructeur par défaut
	 * @return[NONE]
	 */
	public Pos()
	{
	}


	/***********************************************************************//*!
	 * Contructeur par défaut avec initialisation
	 * @param[in] x		La position X
	 * @param[in] y		La position Y
	 * @return[NONE]
	 */
	public Pos( int x, int y )
	{
		m_x = x;
		m_y = y;
	}


	/***********************************************************************//*!
	 * Permet d'obtenir la position X
	 * @return La position X
	 */
	public int x()
	{
		return m_x;
	}


	/***********************************************************************//*!
	 * Permet d'obtenir la position Y
	 * @return La position Y
	 */
	public int y()
	{
		return m_y;
	}


	/***********************************************************************//*!
	 * Permet de définir la position X
	 * @param[in] x 	La nouvelle position X
	 * @return[NONE]
	 */
	public void setX( int x )
	{
		m_x = x;
	}


	/***********************************************************************//*!
	 * Permet de définir la position Y
	 * @param[in] y 	La nouvelle position Y
	 * @return[NONE]
	 */
	public void setY( int y )
	{
		m_y = y;
	}


	/***********************************************************************//*!
	 * Permet de définir la nouvelle position X,Y
	 * @param[in] p 	La nouvelle position X,Y
	 * @return[NONE]
	 */
	public void setPos( final Pos p )
	{
		m_x = p.m_x;
		m_y = p.m_y;
	}


	/***********************************************************************//*!
	 * Permet de tester l'egalité de 2 positions
	 * @param[in] p 	La position a comparer
	 * @return TRUE si les positions sont identiques. FALSE sinon
	 */
	public boolean equal( final Pos p )
	{
		return p.m_x == m_x && p.m_y == m_y;
	}


	/***********************************************************************//*!
	 * Permet de tester l'egalité de 2 positions
	 * @param[in] p 	La position a comparer
	 * @return TRUE si les positions sont identiques. FALSE sinon
	 */
	public boolean equals( final Pos p )
	{
		return p.m_x == m_x && p.m_y == m_y;
	}


	/***********************************************************************//*!
	 * Permet de tester l'egalité de 2 positions
	 * @param[in] p 	La position a comparer
	 * @return TRUE si les positions sont identiques. FALSE sinon
	 */
	@Override
	public boolean equals( final Object p )
	{
		return ((Pos)p).m_x == m_x && ((Pos)p).m_y == m_y;
	}

	/***********************************************************************//*!
	 * Permet de tester l'egalité de 2 positions
	 * @param[in] x 	La position X a comparer
	 * @param[in] y 	La position Y a comparer
	 * @return TRUE si les positions sont identiques. FALSE sinon
	 */
	public boolean equal( int x, int y )
	{
		return x == m_x && y == m_y;
	}


	/***********************************************************************//*!
	 * Permet de tester l'egalité de 2 positions
	 * @param[in] x 	La position X a comparer
	 * @param[in] y 	La position Y a comparer
	 * @return TRUE si les positions sont identiques. FALSE sinon
	 */
	public boolean equals( int x, int y )
	{
		return x == m_x && y == m_y;
	}


	/***********************************************************************//*!
	 * Permet de convertir une position en chaine de caractère
	 * @return Une chaine de caractère.
	 */
	@Override
	public String toString()
	{
		return "("+Integer.toString(m_x)+";"+Integer.toString(m_y)+")";
	}


	/***********************************************************************//*!
	 * Permet de tester si la position {p} est dans la liste a
	 * @param[in] a 	La liste a analyser
	 * @param[in] p 	La position a chercher
	 * @return TRUE si une position identique à {p} est trouvée dans la liste {a}
	 */
	public static boolean isInList( final ArrayList<Pos> a, final Pos p )
	{
		for( int i=0; i<a.size(); i++ )
			if( a.get(i).equal(p) )
				return true;
		return false;
	}


	/***********************************************************************//*!
	 * Permet d'obtenir la position de {p} dans la liste {a}.
	 * @param[in] a 	La liste a analyser
	 * @param[in] p 	La position a chercher
	 * @return -1 Si pas trouvé. >=0 Sinon.
	 */
	public static int findInList( final ArrayList<Pos> a, final Pos p )
	{
		for( int i=0; i<a.size(); i++ )
			if( a.get(i).equal(p) )
				return i;
		return -1;
	}
}
