/***************************************************************************//*!
 * Permet de gérer des données sur un int.
 * Soit 3 données:
 * - 1 bit pour la Case visité
 * - 2 bit pour pour les cases filles
 * - 32-2 bits pour le nombre
 *
 * Permet de sérialiser les données {num}, {sistersCases} et {isRoad} en UN SEUL INT
 *
 * @note Classe static => non instanciable.
 * @bug La classe semble fonctionner mais engendre des bug alors qu'elle passe
 * TOUT les tests inimaginable. ELLE EST DONC DÉSACTIVÉE !
 */
public final class BitManager
{
	/***********************************************************************//*!
	 * On empêche toute création d'instance
	 * @return[NONE]
	 */
	private BitManager(){}


	/***********************************************************************//*!
	 * Permet d'obtenir le nombre réel contenu dans data
	 * @param[in] data		Les données sérialisées
	 * @return Le nombre extrait des données
	 */
	public static int getNumber( int data )
	{
		return data;
		//return (((data/2)/2)/2);
	}


	/***********************************************************************//*!
	 * Permet de définir le nombre a sérialiser dans data
	 * @param[in] data		Les données a modifier
	 * @param[in] num		Le nombre a sérialiser
	 * @return Les nouvelles données sérialisées
	 * @throws Exception Si num < 0 || num > 2^29
	 */
	public static int setNumber( int data, int num ) throws Exception
	{
		if( num < 0 || Integer.MAX_VALUE/(2*2*2) < num )
			throw new Exception("number invalide in setNumber(data,"+num+"); MAX VALUE="+(Integer.MAX_VALUE/(2*2*2)));

		return
				  num*2*2*2// Décalage de 3 bits
				+ getSistersCases(data)*2
				+ getRoad(data);
	}


	/***********************************************************************//*!
	 * Permet de définir si la case a été utilisée comme route
	 * @param[in] data			Les données a modifier
	 * @param[in] isRoad		Case utilisée comme route ?
	 * @return Les nouvelles données sérialisées
	 */
	public static int setRoad( int data, boolean isRoad )
	{
		if( isRoad && !isRoad(data) )
			return data+1;
		if( !isRoad && isRoad(data) )
			return data-1;
		return data;
	}


	/***********************************************************************//*!
	 * Permet d'extraire l'information "Case utilisée comme route ?" de data
	 * @param[in] data		Les données a lire
	 * @return 1 Si la case a été utilisée comme route. 0 Sinon
	 */
	public static int getRoad( int data )
	{
		return (data & 1);
	}


	/***********************************************************************//*!
	 * Permet d'extraire l'information "Case utilisée comme route ?" de data
	 * @param[in] data		Les données a lire
	 * @return TRUE Si la case a été utilisée comme route. FALSE Sinon
	 */
	public static boolean isRoad( int data )
	{
		return getRoad(data) != 0;
	}


	/***********************************************************************//*!
	 * Permet de définir le nombre de cases soeurs.
	 * @param[in] data				les données a modifier
	 * @param[in] sistersCases		Nombre de cases soeurs
	 * @return Les nouvelles données sérialisées
	 * @throws Exception Si sistersCases > 4 || sistersCases < 0
	 */
	public static int setSistersCases( int data, int sistersCases ) throws Exception
	{
		if( sistersCases > 4 || sistersCases < 0 )
			throw new Exception("sistersCases invalide in setSistersCases(data,"+sistersCases+");");

		return
				  getNumber(data)*2*2*2
				+ sistersCases*2
				+ getRoad(data);
	}


	/***********************************************************************//*!
	 * Permet d'obtenir le nombre de cases soeurs
	 * @param[in] data	Les données sérialisées
	 * @return Le nombre de cases soeurs
	 */
	public static int getSistersCases( int data )
	{
		return ((data & 4) + (data & 2))/2;
	}


	/***********************************************************************//*!
	 * Permet de convertir les données en chaîne de caractère
	 * @param[in] data	Les données sérialisées
	 * @return La chaîne de caractères associée
	 */
	public static String toString( int data )
	{
		return Integer.toString(getNumber(data))+" "+Integer.toString(getSistersCases(data))+" "+isRoad(data);
	}


	/***********************************************************************//*!
	 * Permet de sérialiser les données {num}, {sistersCases} et {isRoad} en UN SEUL INT
	 * @param[in] num		Le nombre a sérialiser
	 * @param[in] sistersCases	Le nombre de cases soeurs
	 * @param[in] isRoad		La case est une route ?
	 * @return Les données sérialisées
	 * @throws Exception
	 */
	public static int setData( int num, int sistersCases, boolean isRoad ) throws Exception
	{
		return num;
		/*
		if( sistersCases > 4 || sistersCases < 0 )
			throw new Exception("sistersCases invalide in setSistersCases(data,"+sistersCases+");");

		if( num < 0 || Integer.MAX_VALUE/(2*2*2) < num )
			throw new Exception("num invalide in setNumber(data,"+num+"); MAX VALUE="+(Integer.MAX_VALUE/(2*2*2)));

		int data = num*2*2*2
				+ sistersCases*2
				+ (isRoad?1:0);

		if( getNumber(data) != num || getSistersCases(data) != sistersCases || isRoad(data) != isRoad ){
			System.out.println("Perte de donnée ! num="+num+", sistersCases="+sistersCases+", isRoad="+isRoad
					+", data="+data
					+", num="+getNumber(num)
					+", sistersCases="+getSistersCases(num)
					+", isRoad="+isRoad(num)
			);
			System.exit(42);
		}

		return data;
		//*/
	}


	/***********************************************************************//*!
	 * Jeux de tests pour cette class
	 * @return[NONE]
	 * @throws Exception Une erreur en cas d'echec
	 */
	public static void tests() throws Exception
	{
		int data = BitManager.setData(42, 2, true);
		if( isRoad(data) != true )
			throw new Exception("1. Road lost ! "+data);
		if( getSistersCases(data) != 2 )
			throw new Exception("1. SistersCases lost !");
		if( getNumber(data) != 42 )
			throw new Exception("1. Number lost !");

		data = setRoad(data, false);
		data = setSistersCases(data, 3);
		data = setNumber(data, 15891);
		data = setRoad(data, true);

		if( isRoad(data) != true )
			throw new Exception("2. Road lost !");
		if( getSistersCases(data) != 3 )
			throw new Exception("2. SistersCases lost !");
		if( getNumber(data) != 15891 )
			throw new Exception("2. Number lost !");
	}
}
