import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

/***************************************************************************//*!
 * Point d'entrée. Ne contient que le main, ou des fonctions généralistes
 */
public class Main_EntryPoint
{
	static boolean isConsoleMode = true;

	/***********************************************************************//*!
	 * Main
	 * @param args	Arguments...
	 * 				Les arguments peuvent paramètrer le programmes.
	 * 				Arguments valides:
	 * 				- test					Lance les tests unitaires
	 * 				- isConsoleMode=[BOOL]	Passer en mode console ? (default:false => GUI mode)
	 * 				- viewStep=[BOOL]		Afficher l'animation de génération du Laby
	 * 				- speed=[INT]			Vitesse d'animation ( >= 0 ) ( Plus la valeur est petite, plus l'animation est rapide )
	 * 				- width=[INT]			Largeur du Laby ( > 0 )
	 * 				- height=[INT]			Hauteur du Laby ( > 0 )
	 * 				- file=[String]			Fichier a charger. ( *file*= peut être omis )
	 * 				- behavior=[String]		Type d'algo a utiliser: BestFirst, ASearch, UniformCost
	 * 				- help					Affiche l'aide en cours
	 * @return[NONE]
	 *
	 * Exemple d'utilisation:
	 * @code
	 * 		System.out.println("\tProg width=10 height=10 speed=250");
	 * @endcode
	 */
	public static void main( final String[] args )
	{
		if( (Arrays.binarySearch(args, "test") >= 0) ){// Arrays.binarySearch equivalent de in_array en php
			try{
				isConsoleMode = true;
				System.out.print("Test Laby:\t\t");
				Laby.tests();
				Console.print("[OK]\n", Console.Green | Console.Highlight);
				System.out.print("Test Ants:\t\t");
				Ants.tests();
				Console.print("[OK]\n", Console.Green | Console.Highlight);
				System.out.print("Test BitManager:\t");
				BitManager.tests();
				Console.print("[OK]\n", Console.Green | Console.Highlight);
			}catch( Exception e ){
				Console.print("[Fail]\n", Console.Red | Console.Highlight);
				System.out.println("Error:\t\t"+e.getMessage());
			}
			return ;
		}


		/***********************************************************************
		 * Récupération des paramètres via ligne de commande
		 */
		for( int i=0; i<args.length; i++ )
		{
			if( args[i].regionMatches(0, "viewStep=", 0, 9) || args[i].regionMatches(0, "viewstep=", 0, 9)){
				Simulation.viewStep = Boolean.parseBoolean(args[i].substring(9));

			}else if( args[i].regionMatches(0, "speed=", 0, 6) ){
				Simulation.speed = Integer.parseInt(args[i].substring(6));
				if( Simulation.speed < 0 )
					Simulation.speed = 0;

			}else if( args[i].regionMatches(0, "width=", 0, 6) ){
				Simulation.width = Integer.parseInt(args[i].substring(6));

			}else if( args[i].regionMatches(0, "height=", 0, 7) ){
				Simulation.height = Integer.parseInt(args[i].substring(7));

			}else if( args[i].regionMatches(0, "file=", 0, 5) ){
				Simulation.filename = args[i].substring(5);

			}else if( args[i].regionMatches(0, "behavior=", 0, 9) ){
				String tmp = args[i].substring(9);
				if( tmp.equals("BestFirst") ){
					Simulation.behavior = Behavior.type.BestFirst;
				}else if( tmp.equals("ASearch") ){
					Simulation.behavior = Behavior.type.ASearch;
				}else if( tmp.equals("UniformCost") ){
					Simulation.behavior = Behavior.type.UniformCost;
				}else{
					System.out.println("Valeur incorrect pour behavior ! "+tmp);
					System.exit(1);
				}

			}else if( args[i].regionMatches(0, "isConsoleMode=", 0, 14) || args[i].regionMatches(0, "isconsolemode=", 0, 14) ){
				Main_EntryPoint.isConsoleMode = Boolean.parseBoolean(args[i].substring(14));

			}else if( args[i].startsWith("help") || args[i].startsWith("?") || args[i].startsWith("--help") || args[i].startsWith("/?") || args[i].startsWith("/help") ){
				System.out.println("Liste des commandes possibles:");
				System.out.println("\tisConsoleMode=[BOOL]\tPasser en mode console ? (default:false => GUI mode)");
				System.out.println("\tviewStep=[BOOL]\t\tAfficher l'animation de génération du Laby");
				System.out.println("\tspeed=[INT]\t\tVitesse d'animation ( >= 0 ) ( Plus la valeur est petite, plus l'animation est rapide )");
				System.out.println("\twidth=[INT]\t\tLargeur du Laby ( > 0 )");
				System.out.println("\theight=[INT]\t\tHauteur du Laby ( > 0 )");
				System.out.println("\tfile=[String]\t\tFichier a charger. (file= peut être homis)");
				System.out.println("\tbehavior=[String]\t\tType d'algo a utiliser: BestFirst, ASearch, UniformCost");
				System.out.println("\ttest\t\t\tExecute divers tests");
				System.out.println("\thelp\t\t\tAffiche l'aide en cours");
				System.out.println("\nExemple d'utilisation:");
				System.out.println("\tProg width=10 height=10 speed=250");
				System.exit(0);
			}else{
				// C'est un fichier
				Simulation.filename = args[i];
			}
		}

		System.out.println("Paramètres utilisé:");
		System.out.println("\tisConsoleMode="+(Main_EntryPoint.isConsoleMode?"true":"false"));
		System.out.println("\tviewStep="+(Simulation.viewStep?"true":"false"));
		System.out.println("\tspeed="+Integer.toString(Simulation.speed));
		System.out.println("\twidth="+Integer.toString(Simulation.width));
		System.out.println("\theight="+Integer.toString(Simulation.height));
		System.out.println("\tfile="+Simulation.filename);
		System.out.println("\tbehavior="+Simulation.behavior);
		System.out.println("Pour plus d'information:");
		System.out.println("\tjava Main_EntryPoint help");

		if( Main_EntryPoint.isConsoleMode ){
			Console.print("\nVeuillez appuier sur Entrer pour continuer.\n", Console.Underline);
			//(new Scanner(System.in)).nextLine();
		}


		/*******************************************************************
		 * Paramétrage et Lancement du laby
		 */
		try {
			if( !Main_EntryPoint.isConsoleMode ){
				Simulation.setShowCaseNumber(false);
				// Création de la window
				GraphicalView fen = new GraphicalView();
				fen.setVisible(true);
				return ;
			}


			/*******************************************************************
			 * Lancement du laby
			 */
			Simulation.play();

		}catch( Exception e ){
			file_put_contents(e);
		}
	}


	/***********************************************************************//*!
	 * Permet de générer un nombre aléatoire compris entre {min} et {max}
	 * @param[in] min	Nombre minimum (compris)
	 * @param[in] max	Nombre maximum (compris)
	 * @return The God of Random: Crôôm
	 */
	public static int rand( int min, int max )
	{
		return (int)(Math.random() * (max-min)) + min;
	}


	/***********************************************************************//*!
	 * Permet de mettre en pause le prog pendant {milli} milli-secondes
	 * @param[in] milli		Durée de la pause
	 * @return[NONE]
	 */
	public static void sleep( long milli )
	{
		try{
		  Thread.sleep(milli);//sleep for {milli} ms
		}catch( InterruptedException ie ){
			// If this thread was interrupted by an other thread
			ie.printStackTrace();
			System.out.println(ie.getMessage());
		}
	}


	/***********************************************************************//*!
	 * Permet de loguer des informations
	 * @param[in] data		Les données a mettre dans le log
	 * @return[NONE]
	 */
	static BufferedWriter out = null;
	public static void file_put_contents( final String data )
	{
		try {
			if( out == null )
				out = new BufferedWriter(new FileWriter("error.log"));
			out.write(data+"\n");
			out.flush();
			//out.close();
		}catch( IOException e ){
			System.out.println("Exception ");
		}
	}


	/***********************************************************************//*!
	 * Permet de loguer une exception
	 * @param[in] e		L'exception a mettre dans le log
	 * @return[NONE]
	 */
	public static void file_put_contents( final Exception e )
	{
		if( isConsoleMode == false )
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage());

		System.out.println(e.getMessage());
		e.printStackTrace();

		file_put_contents(e.getMessage());
		StackTraceElement[] st = e.getStackTrace();
		for( int i=0; i<st.length; i++ )
			file_put_contents(st[i].toString());
	}
}
