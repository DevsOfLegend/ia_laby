import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.text.NumberFormat;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/***************************************************************************//*!
 * Permet de créer une interface GUI (graphique) pour l'app Fourmiz
 * @warning On ne peut créer qu'une seule window !
 * @note Pour lancer l'interface graphique: java Main_EntryPoint isConsoleMode=false
 * @note Cette classe implémente FocusListener pour pouvoir modifier directement
 * les données en sortie d'input. Cette classe implémente aussi ActionListener
 * pour avoir une action sur les JButton.
 */
public class GraphicalView extends JFrame implements FocusListener, ActionListener
{
	// Code pour JPanel
	private static final long serialVersionUID = 1L;//!< Ajout automatique par eclipse...
	private static GraphicalView m_inst = null;//!< Permet de retrouver l'instance. Permet aussi de s'assurer qu'il n'existe qu'une seul window
	private JPanel m_layout = new JPanel();//!< "Grille" de positionnement. Contient tout les widget ( input, label, ... )
	private JButton m_start_Btn = new JButton("Lancer la simulation");//!< Bouton permettant de: lancer la simulation, de la mettre en pause et de reprendre la simulation
	private JButton m_reset = new JButton("Remettre a zero");// Bouton permettant de reset la simulation
	private JComboBox<String> m_loader = new JComboBox<String>();
	private JComboBox<String> m_behavior = new JComboBox<String>();
	private JCheckBox m_showCaseNumber = new JCheckBox("Activer l'affichage des numéros de case.");// Case à cocher permettant d'activer/désactiver l'affichage des num de case
	private JButton m_speedMore = new JButton("+");// Input permettant de modifier la vitesse
	private JButton m_speedLess = new JButton("-");// Input permettant de modifier la vitesse
	private JLabel m_speedLabel = new JLabel(Integer.toString(100-((Simulation.speed*100)/150)+100)+"%");// Input permettant de modifier la vitesse
	private JFormattedTextField m_width = new JFormattedTextField(NumberFormat.getIntegerInstance());// Input permettant de modifier la largeur du laby
	private JFormattedTextField m_height = new JFormattedTextField(NumberFormat.getIntegerInstance());// Input permettant de modifier la hauteur du laby


	/***********************************************************************//*!
	 * Permet de créer une window dotée d'un ardoise
	 * @return[NONE]
	 * @throws Exception Pas de création de 2 window !
	 */
	public GraphicalView() throws Exception
	{
		if( m_inst != null ){
			javax.swing.JOptionPane.showMessageDialog(null, "Vous ne pouvez créer 2 window !");
			System.exit(42);
		}
		m_inst = this;

		// On active l'anti-aliasing pour avoir un texte qui ne brûle pas les yeux
		System.setProperty("awt.useSystemAAFontSettings","on");
		System.setProperty("swing.aatext", "true");

		// On défini le titre de la window
		setTitle("Fourmiz");
		// Taille de la window
		this.setSize(604, 600);
		// On centre la window
		setLocationRelativeTo(null);
		// On autorise le redimensionnement
		setResizable(true);
		// Terminer le processus lorsqu'on clique sur "Fermer"
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		// Système de positionnement par grille
		m_layout.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		/***********************************************************************
		 * Création de l'ardoise
		 * {
		 */
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 0;
			c.weightx = 200;
			c.weighty = 200;
			c.gridwidth = 5;
			m_layout.add(new Board(), c);
		/*
		 * }
		 */

		c = new GridBagConstraints();

		/***********************************************************************
		 * Bouton play/pause
		 * {
		 */
			m_start_Btn.addActionListener(this);
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 1;
			c.weightx = 0.5;
			c.weighty = 0.5;
			c.gridwidth = 3;
			m_layout.add(m_start_Btn, c);
		/*
		 * }
		 */

		/***********************************************************************
		 * Boutons Reset
		 * {
		 */
			m_reset.addActionListener(this);
			m_reset.setEnabled(false);
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 3;
			c.gridy = 1;
			c.weightx = 0.5;
			c.weighty = 0.5;
			c.gridwidth = 2;
			m_layout.add(m_reset, c);
		/*
		 * }
		 */


		/***********************************************************************
		 * ComboBox: Chargeur de laby
		 * {
		 */
			m_loader.addItem("Laby auto gen");
			m_loader.addItem("Laby auto gen (voir etape)");

			File file = new File("./laby/");
			File[] files = file.listFiles();
			for( int fileInList = 0; fileInList<files.length; fileInList++ )
			{
				m_loader.addItem(files[fileInList].toString());

				if( Simulation.filename != null && Simulation.filename.equals(files[fileInList].toString()) )
					m_loader.setSelectedIndex(fileInList+2);
			}

			m_loader.addActionListener(this);
			m_loader.addFocusListener(this);
			if( Simulation.filename == null )
				m_loader.setSelectedIndex(0);
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 2;
			c.weightx = 1;
			c.weighty = 1;
			c.gridwidth = 5;
			m_layout.add(m_loader, c);
		/*
		 * }
		 */

		/***********************************************************************
		 * Algo a utiliser
		 * {
		 */
			m_behavior.addItem("Le meilleur d'abord");
			m_behavior.addItem("A*");
			m_behavior.addItem("Cout uniforme");

			if( Simulation.behavior == Behavior.type.BestFirst )
				m_behavior.setSelectedIndex(0);
			if( Simulation.behavior == Behavior.type.ASearch )
				m_behavior.setSelectedIndex(1);
			if( Simulation.behavior == Behavior.type.UniformCost )
				m_behavior.setSelectedIndex(2);

			m_behavior.addActionListener(this);
			m_behavior.addFocusListener(this);
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 3;
			c.weightx = 1;
			c.weighty = 1;
			c.gridwidth = 5;
			m_layout.add(m_behavior, c);
		/*
		 * }
		 */

		/***********************************************************************
		 * Vitesse d'éxécution
		 * {
		 */
			m_speedMore.addActionListener(this);
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 4;
			c.weightx = 0.5;
			c.weighty = 0.5;
			c.gridwidth = 1;
			m_layout.add(m_speedMore, c);

			c.fill = GridBagConstraints.BOTH;
			c.gridx = 1;
			c.gridy = 4;
			c.weightx = 0.5;
			c.weighty = 0.5;
			c.gridwidth = 1;
			m_layout.add(m_speedLabel, c);
			m_speedLabel.setHorizontalAlignment(JLabel.CENTER);
			m_speedLabel.setVerticalAlignment(JLabel.CENTER);

			m_speedLess.addActionListener(this);
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 2;
			c.gridy = 4;
			c.weightx = 0.5;
			c.weighty = 0.5;
			c.gridwidth = 1;
			m_layout.add(m_speedLess, c);

			// Label
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 3;
			c.gridy = 4;
			c.weightx = 0.5;
			c.weighty = 0.5;
			m_layout.add(new JLabel("Vitesse d'execution"), c);
		/*
		 * }
		 */

		/***********************************************************************
		 * Checkbox: Activation l'affichage numérique des cases ?
		 * {
		 */
			m_showCaseNumber.addActionListener(this);
			m_showCaseNumber.setSelected(Simulation.showCaseNumber());
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 5;
			c.weightx = 1;
			c.weighty = 1;
			c.gridwidth = 5;
			m_layout.add(m_showCaseNumber, c);
		/*
		 * }
		 */

		/***********************************************************************
		 * Largeur du Laby
		 * {
		 */
			// L'input permettant de définir la largeur du Laby
			m_width.setValue(Simulation.width);
			m_width.addActionListener(this);
			m_width.addFocusListener(this);
			if( m_loader.getSelectedIndex() > 1 )
				m_width.setEnabled(false);
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 6;
			c.weightx = 0.5;
			c.weighty = 0.5;
			c.gridwidth = 3;
			m_layout.add(m_width, c);

			// Label
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 3;
			c.gridy = 6;
			c.weightx = 1;
			c.weighty = 1;
			c.gridwidth = 2;
			m_layout.add(new JLabel("Nombre de case pour la largeur du Laby"), c);
		/*
		 * }
		 */

		/***********************************************************************
		 * Hauteur du Laby
		 * {
		 */
			// L'input permettant de définir la hauteur du Laby
			m_height.setValue(Simulation.height);
			m_height.addActionListener(this);
			m_height.addFocusListener(this);
			if( m_loader.getSelectedIndex() > 1 )
				m_height.setEnabled(false);
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 7;
			c.weightx = 0.5;
			c.weighty = 0.5;
			c.gridwidth = 3;
			m_layout.add(m_height, c);

			// Label
			c.fill = GridBagConstraints.BOTH;
			c.gridx = 3;
			c.gridy = 7;
			c.weightx = 0.5;
			c.weighty = 0.5;
			c.gridwidth = 2;
			m_layout.add(new JLabel("Nombre de case pour la hauteur du Laby"), c);
		/*
		 * }
		 */

		setContentPane(m_layout);
	}


	/***********************************************************************//*!
	 * Gestionnaire des events ( après appuis de la touche: entrer )
	 * @param[in] arg0		Contient l'orgine de l'event.
	 * @return[NONE]
	 */
	@Override
	public void actionPerformed( ActionEvent arg0 )
	{
		/***********************************************************************
		 * Bouton play/pause/resume
		 */
		if( arg0.getSource() == m_start_Btn ){
			try{
				Board.cleanScreen(true);
			}catch( Exception e ){
				Main_EntryPoint.file_put_contents(e);
			}
			switch( Simulation.currentStatus() )
			{
				case Started:
					m_start_Btn.setText("Reprendre la simulation");
					Simulation.pause();
					break;

				case Reseted:
				case Stopped:
					m_start_Btn.setText("Mettre en pause");
					m_width.setEnabled(false);
					m_height.setEnabled(false);
					m_loader.setEnabled(false);
					m_behavior.setEnabled(false);
					Simulation.play();
					break;
			}
			m_reset.setEnabled(true);

		/***********************************************************************
		 * Bouton reset
		 */
		}else if( arg0.getSource() == m_reset ){
			m_reset.setEnabled(false);
			m_width.setEnabled(true);
			m_height.setEnabled(true);
			m_loader.setEnabled(true);
			m_behavior.setEnabled(true);
			Simulation.reset();
			// On redraw Le laby de présentation
			try{
				Board.update();
			}catch( Exception e ){
				Main_EntryPoint.file_put_contents(e);
			}
			m_start_Btn.setText("Lancer la simulation");

		/***********************************************************************
		 * Activer les numéros de case ?
		 */
		}else if( arg0.getSource() == m_showCaseNumber ){
			Simulation.setShowCaseNumber(m_showCaseNumber.isSelected());

		/***********************************************************************
		 * Augmenter la vitesse de la simulation
		 */
		}else if( arg0.getSource() == m_speedMore ){
			if( Simulation.speed == 0 )
				return ;
			Simulation.speed -= 10;
			m_speedLabel.setText(Integer.toString(100-((Simulation.speed*100)/150)+100)+"%");

		/***********************************************************************
		 * Diminuer la vitesse de la simulation
		 */
		}else if( arg0.getSource() == m_speedLess ){
			Simulation.speed += 10;
			m_speedLabel.setText(Integer.toString(100-((Simulation.speed*100)/150)+100)+"%");

		/***********************************************************************
		 * Largeur du laby
		 */
		}else if( arg0.getSource() == m_width ){
			int w = Integer.parseInt(m_width.getValue().toString());
			if( w < 2 ){
				w = 2;
				m_width.setValue(w);
				javax.swing.JOptionPane.showMessageDialog(null, "La largeur minimum est de 2 !");
			}else if( w > 200 ){
				w = 200;
				m_width.setValue(w);
				javax.swing.JOptionPane.showMessageDialog(null, "La largeur maximum est de 200 !");
			}
			Simulation.width = w;
			// On redraw Le laby de présentation
			try{
				Board.update();
			}catch( Exception e ){
				Main_EntryPoint.file_put_contents(e);
			}

		/***********************************************************************
		 * Hauteur du laby
		 */
		}else if( arg0.getSource() == m_height ){
			int h = Integer.parseInt(m_height.getValue().toString());
			if( h < 2 ){
				h = 2;
				m_height.setValue(h);
				javax.swing.JOptionPane.showMessageDialog(null, "La hauteur minimum est de 2 !");
			}else if( h > 200 ){
				h = 200;
				m_height.setValue(h);
				javax.swing.JOptionPane.showMessageDialog(null, "La hauteur maximum est de 200 !");
			}

			Simulation.height = h;
			// On redraw Le laby de présentation
			try{
				Board.update();
			}catch( Exception e ){
				Main_EntryPoint.file_put_contents(e);
			}

		/***********************************************************************
		 * Chargeur de laby
		 */
		}else if( arg0.getSource() == m_loader ){
			loadLaby();

		/***********************************************************************
		 * Sélecteur d'algo
		 */
		}else if( arg0.getSource() == m_behavior ){
			if( m_behavior.getSelectedIndex() == 0 )
				Simulation.behavior = Behavior.type.BestFirst;
			if( m_behavior.getSelectedIndex() == 1 )
				Simulation.behavior = Behavior.type.ASearch;
			if( m_behavior.getSelectedIndex() == 2 )
				Simulation.behavior = Behavior.type.UniformCost;
		}
	}


	/***********************************************************************//*!
	 * Gestionnaire des events ( après obtention du focus )
	 * @param[in] arg0		Contient l'origine de l'event.
	 * @return[NONE]
	 *
	 * @note Cette fonction a du être réimplantée a cause de l'interface: FocusListener
	 * Or cette interface est nécessaire pour focusLost().
	 * C'est pourquoi cette fonction est vide.
	 */
	@Override
	public void focusGained( FocusEvent arg0 ){}


	/***********************************************************************//*!
	 * Gestionnaire des events ( après perte du focus )
	 * @param[in] arg0		Contient l'origine de l'event.
	 * @return[NONE]
	 */
	@Override
	public void focusLost( FocusEvent arg0 )
	{
		if( arg0.isTemporary() )
			return ;

		/***********************************************************************
		 * Largeur
		 */
		if( arg0.getSource() == m_width ){
			try{
				int w = Integer.parseInt(m_width.getText());
				if( w < 2 ){
					w = 2;
					m_width.setValue(w);
					javax.swing.JOptionPane.showMessageDialog(null, "La largeur minimum est de 2 !");
				}else if( w > 200 ){
					w = 200;
					m_width.setValue(w);
					javax.swing.JOptionPane.showMessageDialog(null, "La largeur maximum est de 200 !");
				}
				Simulation.width = w;
				Board.update();

			}catch( NumberFormatException e ){
				m_width.requestFocusInWindow();
				javax.swing.JOptionPane.showMessageDialog(null, "La valeur \"largeur du laby\" doit être numérique !");
			}catch( Exception e ){
				Main_EntryPoint.file_put_contents(e);
			}

		/***********************************************************************
		 * Hauteur
		 */
		}else if( arg0.getSource() == m_height ){
			try{
				int h = Integer.parseInt(m_height.getText());
				if( h < 2 ){
					h = 2;
					m_height.setValue(h);
					javax.swing.JOptionPane.showMessageDialog(null, "La hauteur minimum est de 2 !");
				}else if( h > 200 ){
					h = 200;
					m_height.setValue(h);
					javax.swing.JOptionPane.showMessageDialog(null, "La hauteur maximum est de 200 !");
				}
				Simulation.height = h;
				Board.update();

			}catch( NumberFormatException e ){
				m_height.requestFocusInWindow();
				javax.swing.JOptionPane.showMessageDialog(null, "La valeur \"hauteur du laby\" doit être numérique !");
			}catch( Exception e ){
				Main_EntryPoint.file_put_contents(e);
			}


		/***********************************************************************
		 * Chargeur de laby
		 */
		}else if( arg0.getSource() == m_loader ){
			loadLaby();

		/***********************************************************************
		 * Sélecteur d'algo
		 */
		}else if( arg0.getSource() == m_behavior ){
			if( m_behavior.getSelectedIndex() == 0 )
				Simulation.behavior = Behavior.type.BestFirst;
			if( m_behavior.getSelectedIndex() == 1 )
				Simulation.behavior = Behavior.type.ASearch;
			if( m_behavior.getSelectedIndex() == 2 )
				Simulation.behavior = Behavior.type.UniformCost;
		}
	}


	/***********************************************************************//*!
	 * @brief Permet de savoir quand une Simulation s'est terminée
	 * @return[NONE]
	 * @note Cette fonction est appelée par Simulation
	 */
	public static void simulation_end_event()
	{
		m_inst.m_reset.setEnabled(false);
		m_inst.m_width.setEnabled(true);
		m_inst.m_height.setEnabled(true);
		m_inst.m_loader.setEnabled(true);
		m_inst.m_behavior.setEnabled(true);
		m_inst.m_start_Btn.setText("Lancer la simulation");
	}


	/***********************************************************************//*!
	 * @brief Permet de changer de Laby
	 * @return[NONE]
	 */
	private void loadLaby()
	{
		if( m_loader.getSelectedIndex() > 1 ){
			m_width.setEnabled(false);
			m_height.setEnabled(false);

		    File f = new File((String) m_loader.getSelectedItem());
		    if( !f.exists() ){
		    	javax.swing.JOptionPane.showMessageDialog(null, "Le fichier <"+((String) m_loader.getSelectedItem())+"> est introuvable !");
				m_width.setEnabled(true);
				m_height.setEnabled(true);
				m_loader.setSelectedIndex(0);
				return ;
		    }

	    	Simulation.filename = (String) m_loader.getSelectedItem();
		    return ;
		}
		m_width.setEnabled(true);
		m_height.setEnabled(true);
		Simulation.viewStep = m_loader.getSelectedIndex() == 1;
		Simulation.filename = null;
	}
}
