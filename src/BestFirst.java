import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/***************************************************************************//*!
* Implementation de l'algo "Meilleur d'abord"
*/
public class BestFirst implements Behavior
{

	private LinkedList<Pos> m_cases = null;	//!<Cases du labyrinthe à visiter
	private int m_penality = 0;				//!<Nombre de pénalité pendant le parcours du labyrinthe
	private int m_nbrOfStrokes = 1;			//!<Nombre de coups joués avant la résolution du labyrinthe
	private ArrayList<Pos> m_output = null;	//!<Liste des sortie du labyrinthe
	private boolean m_nothingValid = false;	//!<Permet d'indiquer si rien de valide n'a été trouvé
	private ArrayList<Pos> m_oldPos = null;	//!<Tableau des anciennes positions

	/***********************************************************************//*!
	 * Fonction vérifiant si le couloir du haut est une impasse
	 * @param[in] pos		La position a vérifier
	 * @param[in] l			L'environement
	 * @return TRUE si le couloir est un cul de sac
	 */
	public boolean isTopDeadlock( final Pos pos, final Laby l )
	{
		//on commence par vérifier la case juste au dessus
		Pos tmpPos = new Pos(pos.x(), pos.y());
		try {
			while(true){
				System.out.println("enter into is topdeadlock while ");
				System.out.println("case: "+tmpPos.toString()+" left?:"+l.isLeftWall(tmpPos.x(), tmpPos.y())+", right?:"+l.isRightWall(tmpPos.x(), tmpPos.y()));

				if( l.isTopWall(tmpPos.x(), tmpPos.y()) && l.isRightWall(tmpPos.x(), tmpPos.y()) && l.isLeftWall(tmpPos.x(), tmpPos.y()) )
					break;

				if(!l.isLeftWall(tmpPos.x(), tmpPos.y()) || !l.isRightWall(tmpPos.x(), tmpPos.y()))
					return false;

				//on monte d'un cran dans le labyrinthe
				tmpPos.setY(tmpPos.y()-1);
			}

			System.out.println("TOP deadlock ("+pos.x()+";"+pos.y()+")  / TOP :"+l.getBlockInfo(pos.x(), pos.y()-1)+" BOTTOM :"+l.getBlockInfo(pos.x(), pos.y()+1));
		} catch (Exception e) {
			Main_EntryPoint.file_put_contents(e);
		}
		return true;
	}


	/***********************************************************************//*!
	 * Fonction vérifiant si le couloir du bas est une impasse
	 * @param[in] pos		La position a vérifier
	 * @param[in] l			L'environement
	 * @return TRUE si le couloir est un cul de sac
	 */
	public boolean isBottomDeadlock( final Pos pos, final Laby l)
	{
		//on commence par vérifier la case juste en dessous
		Pos tmpPos = new Pos(pos.x(), pos.y());
		try {
			while( true ){
				if( l.isBottomWall(tmpPos.x(), tmpPos.y()) && l.isRightWall(tmpPos.x(), tmpPos.y()) && l.isLeftWall(tmpPos.x(), tmpPos.y()) )
					break;

				if(!l.isLeftWall(tmpPos.x(), tmpPos.y()) || !l.isRightWall(tmpPos.x(), tmpPos.y()))
					return false;
				//on descend d'un cran dans le labyrinthe
				tmpPos.setY(tmpPos.y()+1);
			}
		} catch (Exception e) {
			Main_EntryPoint.file_put_contents(e);
		}
		System.out.println("BOTTOM deadlock");
		return true;
	}


	/***********************************************************************//*!
	 * Fonction vérifiant si le couloir de droite est une impasse
	 * @param[in] pos		La position a vérifier
	 * @param[in] l			L'environement
	 * @return TRUE si le couloir est un cul de sac
	 */
	public boolean isRightDeadlock( final Pos pos, final Laby l )
	{
		//on commence par vérifier la case juste à droite
		Pos tmpPos = new Pos(pos.x(), pos.y());
		try {
			while( true ){
				if( l.isTopWall(tmpPos.x(), tmpPos.y()) && l.isRightWall(tmpPos.x(), tmpPos.y()) && l.isBottomWall(tmpPos.x(), tmpPos.y()) )
					break;

				if(!l.isTopWall(tmpPos.x(), tmpPos.y()) || !l.isBottomWall(tmpPos.x(), tmpPos.y()))
					return false;

				//on se décale d'un cran vers la droite dans le labyrinthe
				tmpPos.setX(tmpPos.x()+1);
			}
		} catch (Exception e) {
			Main_EntryPoint.file_put_contents(e);
		}
		System.out.println("RIGHT deadlock");
		return true;
	}


	/***********************************************************************//*!
	 * Fonction vérifiant si le couloir de gauche est une impasse
	 * @param[in] pos		La position a vérifier
	 * @param[in] l			L'environement
	 * @return TRUE si le couloir est un cul de sac
	 */
	public boolean isLeftDeadlock( final Pos pos, final Laby l )
	{
		//on commence par vérifier la case juste à gauche
		Pos tmpPos = new Pos(pos.x(), pos.y());
		try {
			while( true ){
				if( l.isTopWall(tmpPos.x(), tmpPos.y()) && l.isBottomWall(tmpPos.x(), tmpPos.y()) && l.isLeftWall(tmpPos.x(), tmpPos.y()) )
					break;

				if(!l.isTopWall(tmpPos.x(), tmpPos.y()) || !l.isBottomWall(tmpPos.x(), tmpPos.y())){
					return false;
				}
				//on se décale d'un cran vers la droite dans le labyrinthe
				tmpPos.setX(tmpPos.x()-1);
			}
		} catch (Exception e) {
			Main_EntryPoint.file_put_contents(e);
		}
		System.out.println("LEFT deadlock");
		return true;
	}


	/***********************************************************************//*!
	 * Permet de classer h
	 * @param[in,out] h		Le tableau a classer. (Taille du tab: 4)
	 * @return Un tableau donnant les indices de H selon un classement numéric
	 * @warning A la sortie de cette fonction, *h* sera déterioré mais non classé
	 *
	 * Explication:<br />
	 * @code
	 * double[] h = {2,1,4,0}
	 * double[] h2 = sortTab(h);
	 * // h = {-1,-1,-1,-1}
	 * // h2 = {3,1,0,2} <- Liste des indices de H mais classé
	 * @endcode
	 */
	public int[] sortTab ( double[] h )
	{
		int[] ret = {-1,-1,-1,-1};
		int posIndex = 0;

		do {
			double max = -1;
			int idx = -1;
			for( int i=0; i<4; i++ )
			{
				if (max<h[i]){
					max = h[i];
					idx = i;
				}
			}
			ret[posIndex] = idx;
			if( idx != -1 )
				h[idx] = -1;
			posIndex++;
		}while( posIndex<4 );
		return ret;
	}


	/***********************************************************************//*!
	 * Fonction permettant de calculer l'évolution d'une fourmi avec l'algorithme du meilleur d'abord
	 * @param[in,out] a		la fourmi a déplacer
	 * @param[in,out] l		Le Laby a utiliser
	 * @return Le status de l'algo
	 */
	@Override
	public Algo_status action( Ants a, Laby l )
	{
		/***********************************************************************
		 * Si on rentre pour la première fois dans la fonction (i.e. début de l'algo)
		 * on y insère la case initiale de la fourmi : case de départ du labyrinthe
		 */

		System.out.println("fourmi courante : ("+a.getX()+";"+a.getY()+")");
		Pos currentPos = new Pos(a.getX(), a.getY());
		if (m_nbrOfStrokes == 1) {
			if (m_cases == null) m_cases = new LinkedList<Pos>();
			if (m_oldPos == null) m_oldPos = new ArrayList<Pos>();
			m_cases.add(currentPos);
			m_oldPos.add(currentPos);
			//on enregistre la liste des sortie du labyrinthe,
			//qui sera appelé à chaque appelé de la fonction
			m_output = l.getOutput();
			//on met la case à 1
			try {
				l.setBlockInfo(currentPos.x(), currentPos.y(), 1);
			} catch (Exception e) {
				Main_EntryPoint.file_put_contents(e);
			}
		}

		if(m_nothingValid) {
			System.out.println("j'ai du faire demi-tour : je retourne sur une autre case fille");
			a.setPosition(m_cases.getFirst().x(), m_cases.getFirst().y());
			m_nothingValid = false;
			if (!(a.getPos().equal(m_oldPos.get(m_oldPos.size()-1))))
				m_oldPos.add(a.getPos());
			else action(a, l);
			System.out.println(m_oldPos.toString());
			return Algo_status.CONTINUE;
		}

		/***********************************************************************
		 * Si la file des cases à traitée est vide :
		 * => on a fini de visiter la labyrinthe sans trouver de sortie
		 */
		if (m_cases.size() == 0) return Algo_status.FAIL;//throw new Exception("Le labyrinthe n'est pas resolvable");

		/*******************************************************************
		 * On vérifie que la position actuelle est égale à l'une des sorties
		 * => si c'est le cas l'algorithme se termine
		 */
		currentPos = m_cases.getFirst();
		Iterator<Pos> iterator = m_output.iterator();
		while (iterator.hasNext()) {
			if (iterator.next().equal(currentPos)) return Algo_status.END;
		}

		/*******************************************************************
		 * => sinon on cherche les cases voisines (filles) valides
		 * (si une case est valide, on l'ajoute à la file des cases à traiter)
		 */
		//une distance ne pourrait pas être aussi grande que l.width()*l.height()+1
		double heuristique[] = new double[4];
		for (int i=0; i<4; i++) heuristique[i] = l.height()*l.width()+1;

		Pos topCase = null;
		Pos bottomCase = null;
		Pos leftCase = null;
		Pos rightCase = null;
		boolean hasIncrement = false;
		try {
			//case voisine du haut
			System.out.print("test entré dans topcase :   ");
			System.out.println(!l.isTopWall(currentPos.x(), currentPos.y()) && !isTopDeadlock(currentPos, l));
			if (!l.isTopWall(currentPos.x(), currentPos.y()) && !isTopDeadlock(currentPos, l)){
				topCase = new Pos(currentPos.x(), currentPos.y()-1);
				//=> si la case n'a pas encore été visitée
				if(l.getBlockInfo(topCase.x(), topCase.y())==0) {
					//on incrémente le nombre de coups
					if (hasIncrement == false) m_nbrOfStrokes++;
					hasIncrement = true;
					//on calcule la distance par rapport à chaque sortie et on prend le minimum
					iterator = m_output.iterator();
					while (iterator.hasNext()) {
						Pos p = iterator.next();
						//calcule de la distance de la case courante à une des sortie
						double distance = Math.sqrt(Math.pow(topCase.x()-p.x(), 2)+Math.pow(topCase.y()-p.y(), 2));
						//on prend le minimum entre la dernier min des distances calculées et la nouvelle distance
						heuristique[0] = heuristique[0]<distance?heuristique[0]:distance;
						System.out.println("case Haut : "+distance+" case("+topCase.x()+";"+topCase.y()+") & exit("+p.x()+""+p.y()+")");
					}
					System.out.println("case Haut MIN: "+ heuristique[0]);
					l.setBlockInfo(topCase.x(), topCase.y(), m_nbrOfStrokes+1);
				}
			}
			//case voisine de droite
			System.out.print("test entré dans rightcase :   ");
			System.out.println(!l.isRightWall(currentPos.x(), currentPos.y()) && !isRightDeadlock(currentPos, l));
			if (!l.isRightWall(currentPos.x(), currentPos.y()) && !isRightDeadlock(currentPos, l)){
				rightCase = new Pos(currentPos.x()+1, currentPos.y());//on calcule la distance par rapport à chaque sortie et on prend le minimum
				//=> si la case n'a pas encore été visitée
				if(l.getBlockInfo(rightCase.x(), rightCase.y())==0) {
					//on incrémente le nombre de coups
					if (hasIncrement == false) m_nbrOfStrokes++;
					hasIncrement = true;
					//on calcule la distance par rapport à chaque sortie et on prend le minimum
					iterator = m_output.iterator();
					while (iterator.hasNext()) {
						Pos p = iterator.next();
						//calcule de la distance de la case courante à une des sortie
						double distance = Math.sqrt(Math.pow(rightCase.x()-p.x(), 2)+Math.pow(rightCase.y()-p.y(), 2));
						//on prend le minimum entre la dernier min des distances calculées et la nouvelle distance
						heuristique[1] = heuristique[1]<distance?heuristique[1]:distance;
						System.out.println("case Droite : "+distance+" case("+rightCase.x()+";"+rightCase.y()+") & exit("+p.x()+""+p.y()+")");
					}
					System.out.println("case Droite MIN: "+ heuristique[1]);
					l.setBlockInfo(rightCase.x(), rightCase.y(), m_nbrOfStrokes+1);
				}
			}
			//case voisine du bas
			System.out.print("test entré dans bottomcase :   ");
			System.out.println(!l.isBottomWall(currentPos.x(), currentPos.y()) && !isBottomDeadlock(currentPos, l));
			if (!l.isBottomWall(currentPos.x(), currentPos.y()) && !isBottomDeadlock(currentPos, l)){
				bottomCase = new Pos(currentPos.x(), currentPos.y()+1);
				//=> si la case n'a pas encore été visitée
				if(l.getBlockInfo(bottomCase.x(), bottomCase.y())==0) {
					//on incrémente le nombre de coups
					if (hasIncrement == false) m_nbrOfStrokes++;
					hasIncrement = true;
					//on calcule la distance par rapport à chaque sortie et on prend le minimum
					iterator = m_output.iterator();
					while (iterator.hasNext()) {
						Pos p = iterator.next();
						//calcule de la distance de la case courante à une des sortie
						double distance = Math.sqrt(Math.pow(bottomCase.x()-p.x(), 2)+Math.pow(bottomCase.y()-p.y(), 2));
						//on prend le minimum entre la dernier min des distances calculées et la nouvelle distance
						heuristique[2] = heuristique[2]<distance?heuristique[2]:distance;
						System.out.println("case Bas : "+distance+" case("+bottomCase.x()+";"+bottomCase.y()+") & exit("+p.x()+""+p.y()+")");
					}
					System.out.println("case Bas MIN: "+ heuristique[2]);
					System.out.print("TEST " + heuristique[2] + "<" + heuristique[0]);
					System.out.println(heuristique[2]<heuristique[0]);
					System.out.print("TEST " + heuristique[2] + "<" + heuristique[1]);
					System.out.println(heuristique[2]<heuristique[1]);
					l.setBlockInfo(bottomCase.x(), bottomCase.y(), m_nbrOfStrokes+1);
				}
			}
			//case voisine de gauche
			System.out.print("test entré dans leftcase :    ");
			System.out.println(!l.isLeftWall(currentPos.x(), currentPos.y()) && !isLeftDeadlock(currentPos, l));
			if (!l.isLeftWall(currentPos.x(), currentPos.y()) && !isLeftDeadlock(currentPos, l)){
				leftCase = new Pos(currentPos.x()-1, currentPos.y());
				//=> si la case n'a pas encore été visitée
				if(l.getBlockInfo(leftCase.x(), leftCase.y())==0) {
					//on incrémente le nombre de coups
					if (hasIncrement == false) m_nbrOfStrokes++;
					hasIncrement = true;
					//on calcule la distance par rapport à chaque sortie et on prend le minimum
					iterator = m_output.iterator();
					while (iterator.hasNext()) {
						Pos p = iterator.next();
						//calcule de la distance de la case courante à une des sortie
						double distance = Math.sqrt(Math.pow(leftCase.x()-p.x(), 2)+Math.pow(leftCase.y()-p.y(), 2));
						//on prend le minimum entre la dernier min des distances calculées et la nouvelle distance
						heuristique[3] = heuristique[3]<distance?heuristique[3]:distance;
						System.out.println("case Gauche : "+distance+" case("+leftCase.x()+";"+leftCase.y()+") & exit("+p.x()+""+p.y()+")");
					}
					System.out.println("case Gauche MIN: "+ heuristique[3]);
					l.setBlockInfo(leftCase.x(), leftCase.y(), m_nbrOfStrokes+1);
				}
			}

			System.out.println(m_cases.toString());
			//System.out.println("----------pas encore arrivé---------");
			//System.exit(1);

			/*******************************************************************
			 * On compte le nombre de cases valides
			 */
			System.out.println("heuristiques : "+heuristique[0]+" "+heuristique[1]+" "+heuristique[2]+" "+heuristique[3]);
			m_nothingValid = true;
			for (int i=0; i<4; i++)
				if (heuristique[i] != l.width()*l.height()+1)
					m_nothingValid = false;

			/*******************************************************************
			 * Si aucune des cases n'est valide, on fait demi-tour :
			 */
			if(m_nothingValid) {
				System.out.println("m_nothingValid : "+m_nothingValid);
				//on ajoute une pénalité au parcours
				m_penality++;
				//si il n'y a plus de cases mère on sort du programme :
				if (m_oldPos.size()<2) return Algo_status.FAIL;
				//on retourne sur la case mere
				a.setPosition(m_oldPos.get(m_oldPos.size()-2));
				m_oldPos.remove(m_oldPos.size()-1);
				//on dépile la case à partir de laquelle on ne peut explorer
				m_cases.removeFirst();
			}
			/*******************************************************************
			 * Sinon les classes dans la file en fonctions de leurs heuristiques
			 */
			else {
				System.out.println("m_nothingValid : "+m_nothingValid);
				System.out.println("m_cases.getFirst : ("+m_cases.getFirst().x()+";"+m_cases.getFirst().y()+")");

				double h[] = new double[4];
				for (int i=0; i<4; i++) {
					if (heuristique[i] == l.height()*l.width()+1) h[i]=-1;
					else h[i]=heuristique[i];
				}
				int[] sortIndex = sortTab (h);
				for (int i=0; i<4; i++) {
					if (sortIndex[i]==-1) break;
					if (sortIndex[i]==0) m_cases.addFirst(topCase);
					if (sortIndex[i]==1) m_cases.addFirst(rightCase);
					if (sortIndex[i]==2) m_cases.addFirst(bottomCase);
					if (sortIndex[i]==3) m_cases.addFirst(leftCase);
				}

				a.setPosition(m_cases.getFirst());
				m_oldPos.add(a.getPos());
			}
			System.out.println(m_oldPos.toString());
		} catch (Exception e) {
			Main_EntryPoint.file_put_contents(e);
		}

		return Algo_status.CONTINUE;
	}


	/***********************************************************************//*!
	 * Permet d'effectuer des colorations particulières pour la fourmi qui utilise
	 * cet algo
	 * @return[NONE]
	 */
	@Override
	public void draw()
	{
		/***********************************************************************
		 * Affichage en mode console
		 */
		if( Main_EntryPoint.isConsoleMode ){
			Console.make(Console.Red | Console.Highlight);
			return ;
		}

		/***********************************************************************
		 * Affichage en mode GUI
		 */
		try {
			Board.setHorizontalColor(Color.RED);
		}catch( Exception e ){
			Main_EntryPoint.file_put_contents(e);
		}
	}
}
