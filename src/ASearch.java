import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;


/***************************************************************************//*!
* Implementation de l'algo "A*"
*/
public class ASearch implements Behavior
{

	private int m_penality = 0;						//!<Nombre de pénalités pendant le parcours du labyrinthe
	private int m_nbrOfStrokes = 1;					//!<Nombre de coups joués avant la résolution du labyrinthe
	private ArrayList<Pos> m_output = null;			//!<Liste des sorties du labyrinthe
	private ArrayList<Pos> m_handledCases = null;	//!<Cases visitées et traiter
	private ArrayList<Pos> m_cases = null;			//!<Cases visitées mais pas encore traitées
	private double[][] m_mapsOfFValue = null;		//!<Contient les valeurs de la fonction F pour chaque case
	private double[][] m_mapsOfGValue = null;		//!<Contient les valeurs de la fonction G pour chaque case
	private int m_g = 0;							//!<Function G


	/***********************************************************************//*!
	 * Fonction vérifiant si le couloir du haut est une impasse
	 * @param[in] pos		La position a vérifier
	 * @param[in] l			L'environement
	 * @return TRUE si le couloir est un cul de sac
	 */
	public boolean isTopDeadlock( final Pos pos, final Laby l )
	{
		//on commence par vérifier la case juste au dessus
		Pos tmpPos = new Pos(pos.x(), pos.y());
		try {
			while(true){
				System.out.println("enter into is topdeadlock while ");
				System.out.println("case: "+tmpPos.toString()+" left?:"+l.isLeftWall(tmpPos.x(), tmpPos.y())+", right?:"+l.isRightWall(tmpPos.x(), tmpPos.y()));

				if( l.isTopWall(tmpPos.x(), tmpPos.y()) && l.isRightWall(tmpPos.x(), tmpPos.y()) && l.isLeftWall(tmpPos.x(), tmpPos.y()) )
					break;

				if(!l.isLeftWall(tmpPos.x(), tmpPos.y()) || !l.isRightWall(tmpPos.x(), tmpPos.y()))
					return false;

				//on monte d'un cran dans le labyrinthe
				tmpPos.setY(tmpPos.y()-1);
			}

			System.out.println("TOP deadlock ("+pos.x()+";"+pos.y()+")  / TOP :"+l.getBlockInfo(pos.x(), pos.y()-1)+" BOTTOM :"+l.getBlockInfo(pos.x(), pos.y()+1));
		} catch (Exception e) {
			Main_EntryPoint.file_put_contents(e);
		}
		return true;
	}


	/***********************************************************************//*!
	 * Fonction vérifiant si le couloir du bas est une impasse
	 * @param[in] pos		La position a vérifier
	 * @param[in] l			L'environement
	 * @return TRUE si le couloir est un cul de sac
	 */
	public boolean isBottomDeadlock( final Pos pos, final Laby l)
	{
		//on commence par vérifier la case juste en dessous
		Pos tmpPos = new Pos(pos.x(), pos.y());
		try {
			while( true ){
				if( l.isBottomWall(tmpPos.x(), tmpPos.y()) && l.isRightWall(tmpPos.x(), tmpPos.y()) && l.isLeftWall(tmpPos.x(), tmpPos.y()) )
					break;

				if(!l.isLeftWall(tmpPos.x(), tmpPos.y()) || !l.isRightWall(tmpPos.x(), tmpPos.y()))
					return false;
				//on descend d'un cran dans le labyrinthe
				tmpPos.setY(tmpPos.y()+1);
			}
		} catch (Exception e) {
			Main_EntryPoint.file_put_contents(e);
		}
		System.out.println("BOTTOM deadlock");
		return true;
	}


	/***********************************************************************//*!
	 * Fonction vérifiant si le couloir de droite est une impasse
	 * @param[in] pos		La position a vérifier
	 * @param[in] l			L'environement
	 * @return TRUE si le couloir est un cul de sac
	 */
	public boolean isRightDeadlock( final Pos pos, final Laby l )
	{
		//on commence par vérifier la case juste à droite
		Pos tmpPos = new Pos(pos.x(), pos.y());
		try {
			while( true ){
				if( l.isTopWall(tmpPos.x(), tmpPos.y()) && l.isRightWall(tmpPos.x(), tmpPos.y()) && l.isBottomWall(tmpPos.x(), tmpPos.y()) )
					break;

				if(!l.isTopWall(tmpPos.x(), tmpPos.y()) || !l.isBottomWall(tmpPos.x(), tmpPos.y()))
					return false;

				//on se décale d'un cran vers la droite dans le labyrinthe
				tmpPos.setX(tmpPos.x()+1);
			}
		} catch (Exception e) {
			Main_EntryPoint.file_put_contents(e);
		}
		System.out.println("RIGHT deadlock");
		return true;
	}


	/***********************************************************************//*!
	 * Fonction vérifiant si le couloir de gauche est une impasse
	 * @param[in] pos		La position a vérifier
	 * @param[in] l			L'environement
	 * @return TRUE si le couloir est un cul de sac
	 */
	public boolean isLeftDeadlock( final Pos pos, final Laby l )
	{
		//on commence par vérifier la case juste à gauche
		Pos tmpPos = new Pos(pos.x(), pos.y());
		try {
			while( true ){
				if( l.isTopWall(tmpPos.x(), tmpPos.y()) && l.isBottomWall(tmpPos.x(), tmpPos.y()) && l.isLeftWall(tmpPos.x(), tmpPos.y()) )
					break;

				if(!l.isTopWall(tmpPos.x(), tmpPos.y()) || !l.isBottomWall(tmpPos.x(), tmpPos.y())){
					return false;
				}
				//on se décale d'un cran vers la droite dans le labyrinthe
				tmpPos.setX(tmpPos.x()-1);
			}
		} catch (Exception e) {
			Main_EntryPoint.file_put_contents(e);
		}
		System.out.println("LEFT deadlock");
		return true;
	}


	/***********************************************************************//*!
	 * Permet d'obtenir la case de meilleur "qualité" (heuristique)
	 * @param[in] mapsOfFValue		Contient la distance parcouru + distance vers la plus proche sortie
	 * @param cases					Liste des cases connues a traiter
	 * @param width					Taille du tab de mapsOfFValue
	 * @param height				Taille du tab de mapsOfFValue
	 * @return La position de la meilleur case
	 */
	public Pos getBestCase ( double[][] mapsOfFValue, ArrayList<Pos> cases, int width, int height )
	{
		Pos res = new Pos();
		double min = width*height*2+1;
		for (int i=0; i<cases.size(); i++) {
			double tmp = mapsOfFValue[cases.get(i).x()][cases.get(i).y()];
			if (min>tmp) {
				min = tmp;
				//on enregistre la case de plus petite valeur
				res.setX(cases.get(i).x());
				res.setY(cases.get(i).y());
			}
		}
		return res;
	}


	/***********************************************************************//*!
	 * Fonction permettant de calculer l'évolution d'une fourmi avec l'algorithme A*
	 * @param[in,out] a		la fourmi a déplacer
	 * @param[in,out] l		Le Laby a utiliser
	 * @return Le status de l'algo
	 */
	@Override
	public Algo_status action( Ants a, Laby l )
	{
		/***********************************************************************
		 * initialisation de la recherche
		 */
		if (m_nbrOfStrokes == 1) {
			m_handledCases = new ArrayList<Pos>();
			m_cases = new ArrayList<Pos>();
			m_output = new ArrayList<Pos>();
			m_output = l.getOutput();
			m_mapsOfFValue = new double[l.width()][l.height()];
			m_mapsOfGValue = new double[l.width()][l.height()];
			for (int i=0; i<l.width();i++){
				for (int j=0; j<l.height(); j++){
					m_mapsOfFValue[i][j] = 0;
					m_mapsOfGValue[i][j] = 0;
				}
			}
			//on ajoute la case de départ au tableau des cases connues à traiter
			m_cases.add(a.getPos());
		}

		/***********************************************************************
		 * Si la liste des cases à traiter est vide, l'algo est terminé
		 */
		if (m_cases.size()==0) return Algo_status.FAIL;

		/***********************************************************************
		 * on cherche la meilleur case connue a traiter
		 */
		System.out.println(m_cases.toString());
		Pos currentPos = getBestCase(m_mapsOfFValue, m_cases, l.width(), l.height());
		//on supprime la case de la liste de cases à traiter
		int idx = Pos.findInList(m_cases, currentPos);
		if( idx != -1 )
			m_cases.remove(idx);
		//on l'ajoute à la liste des cases déjà traitées
		m_handledCases.add(currentPos);

		/***********************************************************************
		 * on teste si la case courante est une sortie
		 */
		Iterator<Pos> iterator = m_output.iterator();
		while (iterator.hasNext()) {
			if (iterator.next().equal(currentPos)) return Algo_status.END;
		}

		/***********************************************************************
		 * on travaille sur les cases de la cases courantes
		 */
		/*******************************************************************
		 * => sinon on cherche les cases voisines (filles) valides
		 * (si une case est valide, on l'ajoute à la file des cases à traiter)
		 */
		//une distance ne pourrait pas être aussi grande que l.width()*l.height()+1
		double heuristique[] = new double[4];
		for (int i=0; i<4; i++) heuristique[i] = l.height()*l.width()+1;

		Pos topCase = null;
		Pos bottomCase = null;
		Pos leftCase = null;
		Pos rightCase = null;
		boolean hasIncrement = false;
		try {
			//case voisine du haut
			System.out.print("test entré dans topcase :   ");
			System.out.println(!l.isTopWall(currentPos.x(), currentPos.y()) && !isTopDeadlock(currentPos, l));
			if (!l.isTopWall(currentPos.x(), currentPos.y()) && !isTopDeadlock(currentPos, l)){
				topCase = new Pos(currentPos.x(), currentPos.y()-1);
				//si la case n'a pas encore été traitée, on la traite
				if(!m_handledCases.contains(topCase) || m_g<=m_mapsOfGValue[topCase.x()][topCase.y()] && l.getBlockInfo(topCase.x(), topCase.y())==0) {
					//on incrémente le nombre de coups
					if (hasIncrement == false) {
						m_nbrOfStrokes++;
						m_g++;
					}
					hasIncrement = true;
					//on calcule la distance par rapport à chaque sortie et on prend le minimum
					iterator = m_output.iterator();
					while (iterator.hasNext()) {
						Pos p = iterator.next();
						//calcule de la distance de la case courante à une des sortie
						double distance = Math.sqrt(Math.pow(topCase.x()-p.x(), 2)+Math.pow(topCase.y()-p.y(), 2));
						//on prend le minimum entre la dernier min des distances calculées et la nouvelle distance
						heuristique[0] = heuristique[0]<distance?heuristique[0]:distance;
						System.out.println("case Haut : "+distance+" case("+topCase.x()+";"+topCase.y()+") & exit("+p.x()+""+p.y()+")");
					}
					//ajout des valeur g(x) et h(x) au tableaux de valeurs
					m_mapsOfGValue[topCase.x()][topCase.y()] = m_g;
					m_mapsOfFValue[topCase.x()][topCase.y()] = m_g + heuristique[0];
					//ajout de la case a la liste des cases connue, à traiter
					if (!Pos.isInList(m_cases, topCase)) m_cases.add(topCase);
					System.out.println("case Haut MIN: "+ heuristique[0]);
					l.setBlockInfo(topCase.x(), topCase.y(), m_nbrOfStrokes+1);
				}
			}
			//case voisine de droite
			System.out.print("test entré dans rightcase :   ");
			System.out.println(!l.isRightWall(currentPos.x(), currentPos.y()) && !isRightDeadlock(currentPos, l));
			if (!l.isRightWall(currentPos.x(), currentPos.y()) && !isRightDeadlock(currentPos, l)){
				rightCase = new Pos(currentPos.x()+1, currentPos.y());//on calcule la distance par rapport à chaque sortie et on prend le minimum
				//si la case n'a pas encore été traitée on la traite
				if(!m_handledCases.contains(rightCase)  || m_g<=m_mapsOfGValue[rightCase.x()][rightCase.y()] && l.getBlockInfo(rightCase.x(), rightCase.y())==0) {
					//on incrémente le nombre de coups
					if (hasIncrement == false) {
						m_nbrOfStrokes++;
						m_g++;
					}
					hasIncrement = true;
					//on calcule la distance par rapport à chaque sortie et on prend le minimum
					iterator = m_output.iterator();
					while (iterator.hasNext()) {
						Pos p = iterator.next();
						//calcule de la distance de la case courante à une des sortie
						double distance = Math.sqrt(Math.pow(rightCase.x()-p.x(), 2)+Math.pow(rightCase.y()-p.y(), 2));
						//on prend le minimum entre la dernier min des distances calculées et la nouvelle distance
						heuristique[1] = heuristique[1]<distance?heuristique[1]:distance;
						System.out.println("case Droite : "+distance+" case("+rightCase.x()+";"+rightCase.y()+") & exit("+p.x()+""+p.y()+")");
					}
					//ajout des valeur g(x) et h(x) au tableaux de valeurs
					m_mapsOfGValue[rightCase.x()][rightCase.y()] = m_g;
					m_mapsOfFValue[rightCase.x()][rightCase.y()] = m_g + heuristique[1];
					//ajout de la case a la liste des cases connue, à traiter
					if (!Pos.isInList(m_cases, rightCase)) m_cases.add(rightCase);
					System.out.println("case Droite MIN: "+ heuristique[1]);
					l.setBlockInfo(rightCase.x(), rightCase.y(), m_nbrOfStrokes+1);
				}
			}
			//case voisine du bas
			System.out.print("test entré dans bottomcase :   ");
			System.out.println(!l.isBottomWall(currentPos.x(), currentPos.y()) && !isBottomDeadlock(currentPos, l));
			if (!l.isBottomWall(currentPos.x(), currentPos.y()) && !isBottomDeadlock(currentPos, l)){
				bottomCase = new Pos(currentPos.x(), currentPos.y()+1);
				//si la case n'a pas encore été traitée on la traite
				if(!m_handledCases.contains(bottomCase)  || m_g<=m_mapsOfGValue[bottomCase.x()][bottomCase.y()] && l.getBlockInfo(bottomCase.x(), bottomCase.y())==0) {
					//on incrémente le nombre de coups
					if (hasIncrement == false) {
						m_nbrOfStrokes++;
						m_g++;
					}
					hasIncrement = true;
					//on calcule la distance par rapport à chaque sortie et on prend le minimum
					iterator = m_output.iterator();
					while (iterator.hasNext()) {
						Pos p = iterator.next();
						//calcule de la distance de la case courante à une des sortie
						double distance = Math.sqrt(Math.pow(bottomCase.x()-p.x(), 2)+Math.pow(bottomCase.y()-p.y(), 2));
						//on prend le minimum entre la dernier min des distances calculées et la nouvelle distance
						heuristique[2] = heuristique[2]<distance?heuristique[2]:distance;
						System.out.println("case Bas : "+distance+" case("+bottomCase.x()+";"+bottomCase.y()+") & exit("+p.x()+""+p.y()+")");
					}
					//ajout des valeur g(x) et h(x) au tableaux de valeurs
					m_mapsOfGValue[bottomCase.x()][bottomCase.y()] = m_g;
					m_mapsOfFValue[bottomCase.x()][bottomCase.y()] = m_g + heuristique[2];
					//ajout de la case a la liste des cases connue, à traiter
					if (!Pos.isInList(m_cases, bottomCase)) m_cases.add(bottomCase);
					l.setBlockInfo(bottomCase.x(), bottomCase.y(), m_nbrOfStrokes+1);
				}
			}
			//case voisine de gauche
			System.out.print("test entré dans leftcase :    ");
			System.out.println(!l.isLeftWall(currentPos.x(), currentPos.y()) && !isLeftDeadlock(currentPos, l));
			if (!l.isLeftWall(currentPos.x(), currentPos.y()) && !isLeftDeadlock(currentPos, l)){
				leftCase = new Pos(currentPos.x()-1, currentPos.y());
				//si la case n'a pas encore été traitée on la traite
				if(!m_handledCases.contains(leftCase)  || m_g<=m_mapsOfGValue[leftCase.x()][leftCase.y()] && l.getBlockInfo(leftCase.x(), leftCase.y())==0) {
					//on incrémente le nombre de coups
					if (hasIncrement == false) {
						m_nbrOfStrokes++;
						m_g++;
					}
					hasIncrement = true;
					//on calcule la distance par rapport à chaque sortie et on prend le minimum
					iterator = m_output.iterator();
					while (iterator.hasNext()) {
						Pos p = iterator.next();
						//calcule de la distance de la case courante à une des sortie
						double distance = Math.sqrt(Math.pow(leftCase.x()-p.x(), 2)+Math.pow(leftCase.y()-p.y(), 2));
						//on prend le minimum entre la dernier min des distances calculées et la nouvelle distance
						heuristique[3] = heuristique[3]<distance?heuristique[3]:distance;
						System.out.println("case Gauche : "+distance+" case("+leftCase.x()+";"+leftCase.y()+") & exit("+p.x()+""+p.y()+")");
					}
					//ajout des valeur g(x) et h(x) au tableaux de valeurs
					m_mapsOfGValue[leftCase.x()][leftCase.y()] = m_g;
					m_mapsOfFValue[leftCase.x()][leftCase.y()] = m_g + heuristique[3];
					//ajout de la case a la liste des cases connue, à traiter
					if (!Pos.isInList(m_cases, leftCase)) m_cases.add(leftCase);
					System.out.println("case Gauche MIN: "+ heuristique[3]);
					l.setBlockInfo(leftCase.x(), leftCase.y(), m_nbrOfStrokes+1);
				}
			}
		}catch (Exception e) {
			Main_EntryPoint.file_put_contents(e);
		}
		a.setPosition(currentPos);
		//System.exit(42);

		return Algo_status.CONTINUE;
	}


	/***********************************************************************//*!
	 * Permet d'effectuer des colorations particulières pour la fourmi qui utilise
	 * cet algo
	 * @return[NONE]
	 */
	@Override
	public void draw()
	{
		/***********************************************************************
		 * Affichage en mode console
		 */
		if( Main_EntryPoint.isConsoleMode ){
			Console.make(Console.Red | Console.Highlight);
			return ;
		}

		/***********************************************************************
		 * Affichage en mode GUI
		 */
		try {
			Board.setHorizontalColor(Color.RED);
		}catch( Exception e ){
			Main_EntryPoint.file_put_contents(e);
		}
	}
}
