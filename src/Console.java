/***************************************************************************//*!
 * Permet de manipuler la Console de façon plus poussée
 * @note Un grand merci au Site du Zero:
 * - http://www.siteduzero.com/tutoriel-3-32051-les-flags.html
 * - http://www.siteduzero.com/tutoriel-3-35298-des-couleurs-dans-la-console-linux.html
 *
 * @note Cette class est static, elle n'est pas instanciable.
 */
public final class Console
{
	/***************************************************************************
	 * Liste des flags
	 */
	// Couleurs du texte
	public final static int Black			= 1;
	public final static int Red				= 2;
	public final static int Green			= 4;
	public final static int Yellow			= 8;
	public final static int Blue			= 16;
	public final static int Magenta			= 32;
	public final static int Cyan			= 64;
	public final static int White			= 128;
	// Couleur de fond du texte (background)
	public final static int BG_Black		= 256;
	public final static int BG_Red			= 512;
	public final static int BG_Green		= 1024;
	public final static int BG_Yellow		= 2048;
	public final static int BG_Blue			= 4096;
	public final static int BG_Magenta		= 8192;
	public final static int BG_Cyan			= 16384;
	public final static int BG_White		= 32768;

	public final static int Reset			= 65536;//!< Re-mettre les styles par défaut
	public final static int Highlight		= 131072;//!< Mettre la couleur en cours en super brillant
	public final static int Underline		= 262144;//!< Souligner le texte
	public final static int Blink			= 524288;//!< Faire clignoter le texte ( NE MARCHE PAS dans gnome-terminal )
 	public final static int ReverseColor	= 1048576;//!< Inverse la couleur du background avec celle de la couleur du texte


	/***********************************************************************//*!
	 * Permet d'afficher un texte avec les propriété {@code prop}
	 * @param[in] txt	Texte a afficher
	 * @param[in] prop	Propriété à utiliser
	 * @return[NONE]
	 * @code
	 * Console.print("Trop fort", Console.Cyan | Console.BG_White | Console.Underligne);
	 * @endcode
	 */
	public static void print( final String txt, int prop )
	{
		make(prop);

		System.out.print(txt);
		cleanFont();
	}


	/***********************************************************************//*!
	 * Permet de basculer les styles (couleur, italique , gras ....)  en fonction des propriétés {@code prop}
	 * @param[in] prop	Propriété à utiliser
	 * @return[NONE]
	 * @code
	 * Console.make(Console.Cyan | Console.BG_White | Console.Underligne);
	 * System.out.println("Trop fort");
	 * System.out.print("La couleur est toujours là");
	 * System.cleanFont();
	 * @endcode
	 *
	 * @note Idée originale: http://www.siteduzero.com/tutoriel-3-32051-les-flags.html
	 */
	public static void make( int prop )
	{
		// Propriétés diverses
		if( (prop & Reset) != 0 )
			cleanFont();

		if( (prop & Highlight) != 0 )
			System.out.print("\033[1m");

		if( (prop & Underline) != 0 )
			System.out.print("\033[4m");

		if( (prop & Blink) != 0 )
			System.out.print("\033[5m");

		if( (prop & ReverseColor) != 0 )
			System.out.print("\033[7m");

		// Selection de la couleur
		if( (prop & Black) != 0 )
			System.out.print("\033[30m");
		else if( (prop & Red) != 0 )
			System.out.print("\033[31m");
		else if( (prop & Green) != 0 )
			System.out.print("\033[32m");
		else if( (prop & Yellow) != 0 )
			System.out.print("\033[33m");
		else if( (prop & Blue) != 0 )
			System.out.print("\033[34m");
		else if( (prop & Magenta) != 0 )
			System.out.print("\033[35m");
		else if( (prop & Cyan) != 0 )
			System.out.print("\033[36m");
		else if( (prop & White) != 0 )
			System.out.print("\033[37m");

		// Selection du background
		if( (prop & BG_Black) != 0 )
			System.out.print("\033[40m");
		else if( (prop & BG_Red) != 0 )
			System.out.print("\033[41m");
		else if( (prop & BG_Green) != 0 )
			System.out.print("\033[42m");
		else if( (prop & BG_Yellow) != 0 )
			System.out.print("\033[43m");
		else if( (prop & BG_Blue) != 0 )
			System.out.print("\033[44m");
		else if( (prop & BG_Magenta) != 0 )
			System.out.print("\033[45m");
		else if( (prop & BG_Cyan) != 0 )
			System.out.print("\033[46m");
		else if( (prop & BG_White) != 0 )
			System.out.print("\033[47m");
	}


	/***********************************************************************//*!
	 * Permet d'éffacer l'écran
	 * @return[NONE]
	 */
	public static void cleanScreen()
	{
		//System.out.print("\033c");
		System.out.print("\033[H\033[2J");
	}


	/***********************************************************************//*!
	 * Permet d'éffacer le style en cours
	 * @return[NONE]
	 */
	public static void cleanFont()
	{
		System.out.print("\033[0m");
	}


	/***********************************************************************//*!
	 * Positionne le cursor en x et y
	 * @param[in] x		Position X
	 * @param[in] y		Position Y
	 * @return[NONE]
	 */
	public static void gotoxy( int x, int y )
	{
		System.out.printf("\033[%d;%dH", y, x);
	}
}
