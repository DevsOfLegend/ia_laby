/***************************************************************************//*!
 * Cette interface permet de modifier l'algo de déplacement d'une fourmi
 */
public interface Behavior
{
	/*!
	 * Liste des algo disponible
	 */
	public enum type
	{
		BestFirst,
		ASearch,
		UniformCost
	};


	/*!
	 * Retour des algo
	 */
	public enum Algo_status
	{
		END,
		CONTINUE,
		FAIL
	}


	/***********************************************************************//*!
	 * Permet de déplacer une fourmis et de pauser des phéromones en fonction
	 * de son < Behavior >"Comportement"
	 * @param[in,out] a		La fourmi qui doit agir
	 * @param[in,out] l		L'environement de déplacement
	 * @return Le status de l'algo
	 */
	public Algo_status action( Ants a, Laby l );


	/***********************************************************************//*!
	 * Permet de dessiner un comportement.
	 * @return[NONE]
	 */
	public void draw();
}
