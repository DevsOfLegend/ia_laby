import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;


/***************************************************************************//*!
 * Permet de dessiner le laby, les fourmis ainsi que le routes
 * @warning Il ne peut y avoir qu'une seule instance de l'ardoise (Board) !
 * L'instance UNIQUE de l'ardoise est stockée dans Board.inst pour permettre aux
 * divers objets de dessiner sur l'ardoise.
 * @note Board.inst est utilisée pas Laby, Ants, ...
 */
public class Board extends JPanel
{
	private static final long serialVersionUID = 1L;//!< Code pour JPanel. (généré automatiquement par eclipse)
	private static Board m_inst = null;//!< Instance de l'ardoise
	private static int m_x = 0;//!< Position du curseur
	private static int m_y = 0;//!< Position du curseur
	private static boolean m_doUnderline = true;//!< Doit t'on dessiner une barre en dessous ?
	private static Graphics m_currentDrawer = null;//!< Contient le moteur de dessin actuel
	private static int m_Wrelativ = 0;//!< Largeur d'une case
	private static int m_Hrelativ = 0;//!< Hauteur d'une case
	private static final int m_lineWidth = 2;//!< Taille du style
	private static final int m_margin = 2;//!< Taille de la marge
	private static Laby m_example = null;//!< Laby de présentation
//	private static boolean m_isBuffer = false;//!< Activer le buffer anti scintillement
//	private static BufferCase[][] m_buffer = null;//!< Buffer anti scintillement

/* Désactivé -> Pas encore implémenté
	private static class BufferCase
	{
		public boolean isRightWall = false;
		public boolean isUnderlined = false;
		public boolean isBackgroundColor = false;
		public Color colored = null;
		public boolean isText = false;
	}
//*/

	/***********************************************************************//*!
	 * Constructeur
	 * @return[NONE]
	 * @throws Exception	Il ne peut y avoir qu'une seule instance de l'ardoise (Board) !
	 */
	public Board() throws Exception
	{
		if( m_inst != null )
			throw new Exception("Il ne peut y avoir qu'une seule instance de l'ardoise !");
		m_inst = this;

		// On défini le background
		setBackground(Color.WHITE);
		setMinimumSize(new Dimension(604, 400));

		setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
	}


	/***********************************************************************//*!
	 * Permet d'éffacer l'ardoise. Efface aussi les propriétés en cours
	 * (soulignage, couleur, ...)
	 * @param[in] forceClean	Forcer l'effacement de l'écran ? (Ce paramètre
	 * n'est utilisé que si le buffer est activé ).
	 * @return[NONE]
	 * @throws Exception	Renvoie une exception si l'ardoise n'est pas init
	 */
	public static void cleanScreen( boolean forceClean ) throws Exception
	{
		if( m_inst == null )
			throw new Exception("Ardoise non prète");

		m_Wrelativ = (m_inst.getWidth()-4)/(Simulation.width+0);
		m_Hrelativ = (m_inst.getHeight()-4)/(Simulation.height+0);// La ligne du haut doit être comptabilisée

/*
		if( m_isBuffer && !forceClean ){
			if( m_buffer == null || m_buffer.length != Simulation.height || m_buffer[0].length != Simulation.width ){
				m_buffer = new BufferCase[Simulation.height][Simulation.width];
				for( int x=0, y=0; y<Simulation.height; y++ )
					for( x=0; x<Simulation.width; x++ )
						m_buffer[y][x] = new BufferCase();
			}
		}else
//*/
			pen().clearRect(0, 0, m_inst.getWidth(), m_inst.getHeight());


		m_x = 0;
		m_y = 0;

		cleanFont();
	}


	/***********************************************************************//*!
	 * Permet d'éffacer l'ardoise. Efface aussi les propriétés en cours
	 * (soulignage, couleur, ...)
	 * @return[NONE]
	 * @throws Exception	Renvoie une exception si l'ardoise n'est pas init
	 * @note Equivalent à cleanScreen(false)
	 */
	public static void cleanScreen() throws Exception
	{
		cleanScreen(false);
	}


	/***********************************************************************//*!
	 * Permet d'obtenir une craie pour dessiner sur l'ardoise
	 * @return De quoi dessiner sur l'ardoise
	 * @throws Exception	Renvoie une exception si l'ardoise n'est pas init
	 */
	public static Graphics pen() throws Exception
	{
		if( m_inst == null )
			throw new Exception("Ardoise non prète");

		if( m_currentDrawer != null )
			return m_currentDrawer;

		m_currentDrawer = m_inst.getGraphics();
		return m_currentDrawer;
	}


	/***********************************************************************//*!
	 * Permet d'activer le soulignement
	 * @param[in] u		Activer le soulignement ?
	 * @return[NONE]
	 */
	public static void setUnderline( boolean u )
	{
		m_doUnderline = u;
	}


	/***********************************************************************//*!
	 * Permet d'activer le soulignement
	 * @param[in] c		Couleur du pinceau Horizontal
	 * @return[NONE]
	 * @throws Exception	Renvoie une exception si l'ardoise n'est pas init
	 */
	public static void setHorizontalColor( Color c ) throws Exception
	{
		pen().setColor(c);
	}


	/***********************************************************************//*!
	 * Permet de nettoyer les propriétés ( effet soulignée, Couleurs, ... )
	 * @return[NONE]
	 * @throws Exception	Renvoie une exception si l'ardoise n'est pas init
	 */
	public static void cleanFont() throws Exception
	{
		if( m_inst == null )
			throw new Exception("Ardoise non prète");

		m_doUnderline = false;

		// On défini couleur, et taille du texte
		//pen().setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));// <-- On fait ceci dans l'instance !
		pen().setColor(Color.BLACK);

		// On active l'anti-aliasing pour avoir un texte qui ne brûle pas les yeux
		((Graphics2D)pen()).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		// On choisi l'epaisseur du trait/ du stylo
		((Graphics2D)pen()).setStroke(new BasicStroke(m_lineWidth));
	}


	/***********************************************************************//*!
	 * Permet de dessiner un rectangle en X, Y
	 * @param[in] x		Position haut gauche du rectangle
	 * @param[in] y		Position haut gauche du rectangle
	 * @return[NONE]
	 * @throws Exception	Renvoie une exception si l'ardoise n'est pas init
	 */
	public static void drawRectangle( int x, int y ) throws Exception
	{
		if( m_inst == null )
			throw new Exception("Ardoise non prète");

		pen().fillRect(x*m_Wrelativ+m_margin, y*m_Hrelativ+m_margin, m_Wrelativ, m_Hrelativ);
	}


	/***********************************************************************//*!
	 * Permet de dessiner du texte dans la case en X, Y
	 * @param[in] str	Texte a afficher dans la case
	 * @param[in] x		Position haut gauche du rectangle
	 * @param[in] y		Position haut gauche du rectangle
	 * @return[NONE]
	 * @throws Exception	Renvoie une exception si l'ardoise n'est pas init
	 */
	public static void drawString( final String str, int x, int y ) throws Exception
	{
		if( m_inst == null )
			throw new Exception("Ardoise non prète");
/*
		if( m_isBuffer && (!m_buffer[y][x].isText || !pen().getColor().equals(m_buffer[y][x].colored)) ){
			m_buffer[y][x].colored = pen().getColor();
			m_buffer[y][x].isText = true;
		}else if( m_isBuffer ){
			return ;
		}
//*/
		pen().drawString(str, x*m_Wrelativ+m_margin*2,
				(y+1)*m_Hrelativ-m_margin
					-(m_Hrelativ-16)/2// On centre les fourmis
		);
	}


	/***********************************************************************//*!
	 * Permet de dessiner une ligne. (Déplace le cursor)
	 * @return[NONE]
	 * @throws Exception	Renvoie une exception si l'ardoise n'est pas init
	 */
	public static void drawVerticalLine() throws Exception
	{
		drawVerticalLine( true );
	}


	/***********************************************************************//*!
	 * Permet de dessiner une ligne verticale
	 * @param[in] incPos	Déplacer le cursor ?
	 * @return[NONE]
	 * @throws Exception	Renvoie une exception si l'ardoise n'est pas init
	 */
	public static void drawVerticalLine( boolean incPos ) throws Exception
	{
		if( m_inst == null )
			throw new Exception("Ardoise non prète");

		try{
			pen().drawLine(m_x*m_Wrelativ+m_margin, m_y*m_Hrelativ+m_margin, m_x*m_Wrelativ+m_margin, (m_y+1)*m_Hrelativ+m_margin);
		}catch( java.lang.ClassCastException e ){// Cette erreur peut arriver dans certain cas quand on essaie d'arrêter le thread
			Main_EntryPoint.file_put_contents(e);
		}

		if( m_doUnderline ){
			pen().setColor(Color.BLACK);
			pen().drawLine((m_x-1)*m_Wrelativ+m_margin, (m_y+1)*m_Hrelativ+m_margin, m_x*m_Wrelativ, (m_y+1)*m_Hrelativ+m_margin);
		}

		if( incPos && ++m_x > Simulation.width ){
			m_x = 0;
			m_y++;
		}
	}


	/***********************************************************************//*!
	 * Permet de dessiner une ligne horizontale
	 * @param[in] x			Début de la ligne
	 * @param[in] y			Début de la ligne
	 * @param[in] maxWidth	Faire une ligne qui prenne toute l'ardoise ?
	 * @return[NONE]
	 * @throws Exception	Renvoie une exception si l'ardoise n'est pas init
	 */
	public static void drawHorizontalLine( int x, int y, boolean maxWidth ) throws Exception
	{
		if( m_inst == null )
			throw new Exception("Ardoise non prète");

		if( maxWidth ){
			pen().drawLine(x+m_margin, y+m_margin, m_Wrelativ*Simulation.width, y+m_margin);
			return ;
		}
		pen().drawLine(x+m_margin, y+m_margin, m_x*m_Wrelativ-m_margin, y+m_margin);
	}


	/***********************************************************************//*!
	 * Permet de dessiner du vide
	 * @return[NONE]
	 * @throws Exception	Renvoie une exception si l'ardoise n'est pas init
	 */
	public static void drawEmpty() throws Exception
	{
		if( m_inst == null )
			throw new Exception("Ardoise non prète");

		if( m_doUnderline ){
			pen().setColor(Color.BLACK);
			pen().drawLine((m_x-1)*m_Wrelativ+m_margin, (m_y+1)*m_Hrelativ+m_margin, m_x*m_Wrelativ, (m_y+1)*m_Hrelativ+m_margin);
		}

		if( ++m_x > Simulation.width ){
			m_x = 0;
			m_y++;
		}
	}


	/***********************************************************************//*!
	 * Cette fonction est appelée pour pouvoir dessiner l'ardoise.
	 * Cette fonction est appelée dans les cas suivants:
	 * 		- Resize
	 * 		- First draw
	 * @param[in] g		Le "pinceau" qui va servire a dessiner
	 * @return[NONE]
	 */
	@Override
	public void paintComponent( Graphics g )
	{
		// effacer le graphique
		super.paintComponent(g);
		m_currentDrawer = g;
		m_Wrelativ = (getWidth()-4)/(Simulation.width+0);
		m_Hrelativ = (getHeight()-4)/(Simulation.height+0);// La ligne du haut doit être comptabilisée
		try {
			// Permet d'avoir un effet graphique pour le redim
			if( Simulation.currentStatus() == Simulation.Status.Reseted ){
				cleanScreen();
				// On gen un laby pour faire une simple présentation ( Ce laby est utilisé uniquement pour de l'estétique )
				if( m_example == null || m_example.width() != Simulation.width || m_example.height() != Simulation.height ){
					m_example = new Laby(Simulation.width, Simulation.height, new Pos(Main_EntryPoint.rand(2, Simulation.width-2), Main_EntryPoint.rand(2, Simulation.height-2)));
				}else{
					m_example.draw();
				}
				g.setColor(Color.RED);
				g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
				g.drawString("Fourmiz", getWidth()/2-90, getHeight()/2);

			}else{// Permet de voir le laby lors d'un redim
				Simulation.draw();
			}
		}catch( Exception e ){
			Main_EntryPoint.file_put_contents(e);
		}

		m_x = 0;
		m_y = 0;
		m_currentDrawer = null;
	}


	/***********************************************************************//*!
	 * Permet d'actualiser l'affichage de l'ardoise.
	 * @return[NONE]
	 * @throws Exception	Renvoie une exception si l'ardoise n'est pas init
	 */
	public static void update() throws Exception
	{
		if( m_inst == null )
			throw new Exception("Ardoise non prète");

		m_inst.update(pen());
	}
}
