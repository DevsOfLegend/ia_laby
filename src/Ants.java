
/***************************************************************************//*!
* Cette class représente une fourmi.
*/
public class Ants
{
	private Behavior m_actualBehavior = null;//!< Comportement de la fourmis
	private int m_posX;//!< Position X actuel de la fourmi
	private int m_posY;//!< Position Y actuel de la fourmi
	private int m_OldPosX;//!< Ancienne position X actuel de la fourmi
	private int m_OldPosY;//!< Ancienne position Y actuel de la fourmi


	/***********************************************************************//*!
	 * Constructeur
	 * @param[in] t		Comportement initial de la fourmi
	 * @param[in] x 	Position X de la fourmi
	 * @param[in] y		Position Y de la fourmi
	 * @return[NONE]
	 */
	public Ants( Behavior.type t, int x, int y )
	{
		setBehavior( t );
		m_posX = x;
		m_OldPosX = x;
		m_posY = y;
		m_OldPosY = y;
	}


	/***********************************************************************//*!
	 * Permet de changer le comportement d'une fourmi
	 * @param[in] t		Nouveau comportement
	 * @return[NONE]
	 */
	public void setBehavior( Behavior.type t )
	{
		switch( t )
		{
			case BestFirst:
				m_actualBehavior = new BestFirst();
				break;
			case ASearch:
				m_actualBehavior = new ASearch();
				break;
			case UniformCost:
				m_actualBehavior = new UniformCost();
				break;
			default : break;
		}
	}


	/***********************************************************************//*!
	 * Permet d'obtenir le comportement de la fourmi
	 * @return Comportement actuel
	 */
	public Behavior.type behavior()
	{
		if( m_actualBehavior instanceof BestFirst )
			return Behavior.type.BestFirst;
		if( m_actualBehavior instanceof ASearch )
			return Behavior.type.ASearch;
		return null;
	}


	/***********************************************************************//*!
	 * Permet de placer une fourmi en {@code x}, {@code y}
	 * @param[in] x 	Position X de la fourmi
	 * @param[in] y		Position Y de la fourmi
	 * @return[NONE]
	 * @note L'ancienne position est mise en mémoire.
	 * La fourmi n'est capable que de retenir que 2 positions:
	 * 	- Sa position actuelle
	 * 	- Sa position précédente
	 */
	public void setPosition( int x, int y )
	{
		// On save l'ancienne position
		m_OldPosX = m_posX;
		m_OldPosY = m_posY;
		// On applique la nouvelle position
		m_posX = x;
		m_posY = y;
	}


	/***********************************************************************//*!
	 * Permet de placer une fourmi en **p**
	 * @param[in] p		La nouvelle position de la fourmi
	 * @return[NONE]
	 * @note L'ancienne position est mise en mémoire.
	 * La fourmi n'est capable que de retenir que 2 positions:
	 * 	- Sa position actuelle
	 * 	- Sa position précédente
	 */
	public void setPosition( final Pos p)
	{
		setPosition(p.x(), p.y());
	}


	/***********************************************************************//*!
	 * Renvoie la positon X de la fourmi
	 * @return La positon X de la fourmi
	 */
	public int getX()
	{
		return m_posX;
	}


	/***********************************************************************//*!
	 * Renvoie la positon Y de la fourmi
	 * @return La positon Y de la fourmi
	 */
	public int getY()
	{
		return m_posY;
	}


	/***********************************************************************//*!
	 * Renvoie la positon X,Y de la fourmi
	 * @return La positon X,Y de la fourmi
	 */
	public Pos getPos()
	{
		return new Pos(m_posX,m_posY);
	}


	/***********************************************************************//*!
	 * Renvoie l'ancienne positon X de la fourmi
	 * @return L'ancienne positon X de la fourmi
	 */
	public int oldX()
	{
		return m_OldPosX;
	}


	/***********************************************************************//*!
	 * Renvoie l'ancienne positon Y de la fourmi
	 * @return L'ancienne positon Y de la fourmi
	 */
	public int oldY()
	{
		return m_OldPosY;
	}


	/***********************************************************************//*!
	 * Permet de déplacer une fourmi.
	 * @param[in,out] env	L'environement dans lequel la fourmi évolue.
	 * @return cf Behavior::action(Laby)
	 */
	public Behavior.Algo_status move( Laby env )
	{
		return m_actualBehavior.action(this, env);
	}


	/***********************************************************************//*!
	 * Permet de dessiner la fourmi.
	 * @param[in] env	L'environement dans lequel la fourmi évolue.
	 * @return[NONE]
	 */
	public void draw( final Laby env )
	{
		/***********************************************************************
		 * Affichage console
		 */
		if( Main_EntryPoint.isConsoleMode ){
			// Positionnement de la fourmi
			Console.gotoxy(m_posX*6+2, m_posY+1);
			try{
				if( env.showASCII() && env.isBottomWall(m_posX, m_posY) )
					Console.make(Console.Underline);
			}catch( Exception e ){
				e.printStackTrace();
			}

			Console.make(Console.BG_Magenta);

			// On affiche son type
			m_actualBehavior.draw();
			// Draw de la fourmi
			System.out.print("}o}");
			return ;
		}

		/***********************************************************************
		 * Affichage GUI
		 */
		// On affiche son type
		m_actualBehavior.draw();

		try{
			// Draw de la fourmi
			Board.drawString("}o}", m_posX, m_posY);
		}catch( Exception e ){
			Main_EntryPoint.file_put_contents(e);
		}
	}


	/***********************************************************************//*!
	 * Permet de d'afficher dans la console des informations utiles sur la fourmi
	 * et sur ce qu'elle perçoit.
	 * @param[in] env	L'environement dans lequel la fourmi évolue.
	 * @return[NONE]
	 */
	public void showAntInfo( final Laby env )
	{
		try{
			System.out.println("----------------------------------------------------");
			System.out.print("I'm at X:"+m_posX+", Y:"+m_posY);

			System.out.println("I'm a "+m_actualBehavior.getClass().getName());
			System.out.println("I can see:");

			System.out.println("\tisBottomWall:"+env.isBottomWall(m_posX, m_posY));
			System.out.println("\tisLeftWall:"+env.isLeftWall(m_posX, m_posY));
			System.out.println("\tisRightWall:"+env.isRightWall(m_posX, m_posY));
			System.out.println("\tisTopWall:"+env.isTopWall(m_posX, m_posY));
		}catch( Exception e ){
			Main_EntryPoint.file_put_contents(e);
		}
	}


	/***********************************************************************//*!
	 * Permet de tester cette classe, et voir si l'on ne perd pas des données en
	 * cours de route.
	 * @return[NONE]
	 * @throws Exception	Renvoie une Exception si la class ne passe pas tout les tests
	 */
	public static void tests() throws Exception
	{
		Ants a = new Ants(Behavior.type.BestFirst, 1, 1);// Test d'allocation
		if( a.behavior() != Behavior.type.BestFirst || !(a.m_actualBehavior instanceof BestFirst) )
			throw new Exception("Erreur lors de l'allocation, le comportement de la fourmi est incorrect.");

		a.setPosition(0, 0);
		if( a.oldX() != 1 || a.oldY() != 1 || a.getX() != 0 || a.getY() != 0 )
			throw new Exception("Erreur lors du déplacement de la fourmi.");
	}
}
