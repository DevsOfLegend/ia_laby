import java.awt.Color;

/***************************************************************************//*!
* Implémentation de l'algo "Coût Uniforme"
*/
public class UniformCost implements Behavior
{

	/***********************************************************************//*!
	 * C'est dans cette fonction que l'on implémente le coeur de l'algo.
	 * C'est cette fonction qui sera appelée par la fourmi.
	 * @param[in,out] a		la fourmi a déplacer
	 * @param[in,out] l		Le Laby a utiliser
	 * @return Le status de l'algo
	 */
	@Override
	public Algo_status action( Ants a, Laby l )
	{

		return Algo_status.FAIL;
	}


	/***********************************************************************//*!
	 * Permet d'effectuer des colorations particulières pour la fourmi qui utilise
	 * cet algo
	 * @return[NONE]
	 */
	@Override
	public void draw()
	{
		/***********************************************************************
		 * Affichage en mode console
		 */
		if( Main_EntryPoint.isConsoleMode ){
			Console.make(Console.Red | Console.Highlight);
			return ;
		}

		/***********************************************************************
		 * Affichage en mode GUI
		 */
		try {
			Board.setHorizontalColor(Color.RED);
		}catch( Exception e ){
			Main_EntryPoint.file_put_contents(e);
		}
	}
}
