import java.awt.Color;
import java.util.ArrayList;
import java.util.Scanner;


/***************************************************************************//*!
* Cette class permet de créer et de manipuler un Labyrinthe.
*/
public class Laby
{
	// Taille du laby
	private int m_sizeHeight = 10;//!< Taille en hauteur du laby
	private int m_sizeWidth = 10;//!< Taille en largeur du laby
	private int m_labyCase[][];//!< Tableau de case
	private int m_caseSize = 1; //!< Pour l'espace que prend le numéro de la case dans l'espace (exemple 1 -> m_caseSize =1, pour 100 -> m_caseSize =3)
	private static boolean m_doNotDraw = false;//!< Permet de désactiver l'affichage du laby
	private ArrayList<Pos> m_output = null;//!< Liste des sorties
	private boolean m_showCaseNumber = true;//!< Permet d'afficher les nombres dans cases du laby
	private boolean m_showASCII = true;//!< Permet d'afficher les murs en ASCII mode


	/***********************************************************************//*!
	 * Permet d'initier un laby
	 * @param[in] scan		Un pointeur de lecture de fichier
	 * @param[in] width		Largeur du laby
	 * @param[in] height	Hauteur du Laby
	 * @return[NONE]
	 * @throws Exception	Si width <= 0 OU height <= 0
	 * @warning NE FERME PAS din !
	 */
	public Laby( Scanner scan, int width, int height ) throws Exception
	{
		if( width <= 0 )
			throw new Exception("Laby::Laby(DataInputStream,int,int) width incrorrect value ("+width+")");

		m_sizeWidth = width;

		if( height <= 0 )
			throw new Exception("Laby::Laby(DataInputStream,int,int) height incrorrect value ("+height+")");

		m_sizeHeight = height;
		m_caseSize = Integer.toString(m_sizeWidth*m_sizeHeight).length();

		m_output = new ArrayList<Pos>();

		m_labyCase = new int[m_sizeHeight][m_sizeWidth];

		for( int y=0, x=0; y<m_sizeHeight; y++ )
		{
			for( x=0; x<m_sizeWidth; x++ )
			{
				m_labyCase[y][x] = scan.nextInt();

				// Es ce une sortie ?
				if( m_labyCase[y][x] == -1 ){
					m_output.add(new Pos(x,y));
					m_labyCase[y][x] = 0;// La case d'arrivé ne sera pas un mur
				}else if( m_labyCase[y][x] == 1 ){
					m_labyCase[y][x] = Simulation.WALL_ID;// Coloration des murs
				}
			}
		}
	}


	/***********************************************************************//*!
	 * Permet d'initier un laby
	 * @param[in] width		Largeur du laby
	 * @param[in] height	Hauteur du Laby
	 * @param[in] start		Position de départ de la fourmi
	 * @return[NONE]
	 * @throws Exception	Si width <= 0 OU height <= 0
	 */
	public Laby( int width, int height, final Pos start ) throws Exception
	{
		this( width, height, 0, null, start);
	}


	/***********************************************************************//*!
	 * Permet d'initier un laby
	 * @param[in] width		Largeur du laby
	 * @param[in] height	Hauteur du Laby
	 * @param[in] viewStep	Voir les étapes de génération du laby ? (0: NON, 1:OUI, 2: OUI avec les nombres)
	 * @param[in] output	Liste des sorties (Si null => auto gen)
	 * @param[in] start		Position de départ de la fourmi
	 * @return[NONE]
	 * @throws Exception	Si width <= 0 OU height <= 0
	 */
	public Laby( int width, int height, int viewStep, final ArrayList<Pos> output, final Pos start ) throws Exception
	{
		if( width <= 0 )
			throw new Exception("Laby::Laby(int,int) width incrorrect value ("+width+")");

		m_sizeWidth = width;

		if( height <= 0 )
			throw new Exception("Laby::Laby(int,int) height incrorrect value ("+height+")");

		m_sizeHeight = height;

		m_caseSize = Integer.toString(m_sizeWidth*m_sizeHeight-1).length();

		m_showCaseNumber = viewStep>=2;

		genLaby(viewStep>=1, output, 5, start);
	}


	/***********************************************************************//*!
	 * Permet de générer le laby
	 * @param[in] viewStep	Voir les étapes de génération du laby ?
	 * @param[in] output	Liste des sorties. (null => auto gen)
	 * @param[in] nbSeed	Nombre de graine de génération
	 * @param[in] start		Position de départ de la fourmi
	 * @return[NONE]
	 * @throws
	 * 	InterruptedException	Permet d'interompre le thread de Simulation
	 * 	Exception				En cas d'erreur de génération. OUTPUT incorrect
	 */
	@SuppressWarnings("unchecked")
	public void genLaby( boolean viewStep, final ArrayList<Pos> output, int nbSeed, final Pos start ) throws InterruptedException, Exception
	{
		if( output == null ){
			// Génération des sorties aléatoires
			int nbOutput = Main_EntryPoint.rand(1, 5);
			m_output = new ArrayList<Pos>(nbOutput);
			Pos p=null;
			while( nbOutput > 0 )
			{
				p = null;
				switch(Main_EntryPoint.rand(0, 4))
				{
					case 0:// TOP
						p = new Pos(Main_EntryPoint.rand(1, m_sizeWidth-2), 0);
						break;
					case 1:// RIGHT
						p = new Pos(m_sizeWidth-1, Main_EntryPoint.rand(1, m_sizeHeight-2));
						break;
					case 2:// BOTTOM
						p = new Pos(Main_EntryPoint.rand(1, m_sizeWidth-2),m_sizeHeight-1);
						break;
					case 3:// LEFT
						p = new Pos(0, Main_EntryPoint.rand(1, m_sizeHeight-2));
						break;
				}
				if( p != null && !Pos.isInList(m_output, p) ){
					m_output.add(p);
					nbOutput--;
				}
			}
		}else{
			m_output = (ArrayList<Pos>) output.clone();
			// On check que chaque sortie est unique
			ArrayList<Pos> tmp = new ArrayList<Pos>(m_output.size());
			for( int i=0; i<m_output.size(); i++ )
			{
				if( Pos.isInList(tmp, m_output.get(i)) )
					throw new Exception("2 Sorties se chevauchent !");
				tmp.add(m_output.get(i));
			}
		}

		if( !m_doNotDraw )
			System.out.println("Allocation du Laby");
		m_labyCase = new int[m_sizeHeight][m_sizeWidth];

		if( !m_doNotDraw )
			System.out.println("Génération du Laby "+m_sizeWidth+"x"+m_sizeHeight);


		// On remplis le tableau de mur
		for( int y=0, x=0; y<m_sizeHeight; y++ )
			for( x=0; x<m_sizeWidth; x++ )
				m_labyCase[y][x] = Simulation.WALL_ID;

		// On check la position de départ
		if( start.y() <= 0 || start.y() >= m_sizeHeight || start.x() >= m_sizeWidth || start.x() <= 0 )
			throw new Exception("Position de départ incorrect !");

		// Là où il y a le début, on fait une place
		m_labyCase[start.y()][start.x()] = nbSeed;


		// On pose les sorties
		for( int i=0; i<m_output.size(); i++ )
		{
			if( m_output.get(i).y() < 0 || m_output.get(i).y() >= m_sizeHeight || m_output.get(i).x() < 0 || m_output.get(i).x() >= m_sizeWidth )
				throw new Exception("Incorrect output !");
			m_labyCase[m_output.get(i).y()][m_output.get(i).x()] = i+nbSeed+1;
		}

		// On dépose les graines
		for( int i=0,x=0,y=0; i<nbSeed; )
		{
			x = Main_EntryPoint.rand(0, m_sizeWidth);
			y = Main_EntryPoint.rand(0, m_sizeHeight);

			// On ne créé pas de trou dans les parois externes
			if( isTheWall(x, y) || m_labyCase[y][x] != Simulation.WALL_ID )
				continue;

			m_labyCase[y][x] = i;
			i++;
		}

		// Préaffichage
		this.draw();

		int nbKingdom = nbSeed + m_output.size() + 1;
		boolean needRedraw = true;


		// Maintenant, on se charge de générer le laby
		// tant qu'il y a des graines différentes
		do{
			for( int x=0, y=0; y<m_sizeHeight && nbKingdom > 1; y++ )
			{
				for( x=0; x<m_sizeWidth && nbKingdom > 1; x++ )
				{
					if( m_labyCase[y][x] < 0 )
						continue;
					// On est sur une case vide
					// On cherche si on est collé à une autre case
					// Si c'est le cas, on la mange
					switch( Main_EntryPoint.rand(0, 4) )
					{
						case 0:// TOP
							// On ne sort pas du tableau
							if( y <= 0 )
								break;

							// Fagocitage
							if( m_labyCase[y-1][x] != m_labyCase[y][x] && m_labyCase[y-1][x] != Simulation.WALL_ID ){
								replace(m_labyCase[y-1][x], m_labyCase[y][x]);
								nbKingdom--;
								needRedraw = true;
								break;
							}

							// Avec y=1, on ne crée pas de nouvelle brech
							// Ouverture de mur
							// Note une sortie, peux chercher a s'agrandir
							if( y==m_sizeHeight-1 || (!isTheWall(x,y-1) && !hasMoreThanOne(x,y-1,m_labyCase[y][x])) ){
								m_labyCase[y-1][x] = m_labyCase[y][x];
								needRedraw = true;
							}
							break;

						case 1:// RIGHT
							// On ne sort pas du tableau
							if( x+1 >= m_sizeWidth )
								break;

							// Fagocitage
							if( m_labyCase[y][x+1] != m_labyCase[y][x] && m_labyCase[y][x+1] != Simulation.WALL_ID ){
								replace(m_labyCase[y][x+1], m_labyCase[y][x]);
								nbKingdom--;
								needRedraw = true;
								break;
							}

							// Ouverture de mur
							// On ne créer pas de nouvelle sortie, donc on évite les x=m_sizeWidth-1
							if( x==0 || (!isTheWall(x+1,y) && !hasMoreThanOne(x+1,y,m_labyCase[y][x])) ){
								m_labyCase[y][x+1] = m_labyCase[y][x];
								needRedraw = true;
							}
							break;

						case 2:// BOTTOM
							// On ne sort pas du tableau
							if( y+1 >= m_sizeHeight )
								break;

							// Fagocitage
							if( m_labyCase[y+1][x] != m_labyCase[y][x] && m_labyCase[y+1][x] != Simulation.WALL_ID ){
								replace(m_labyCase[y+1][x], m_labyCase[y][x]);
								nbKingdom--;
								break;
							}

							// Ouverture de mur
							// On ne créer pas de nouvelle sortie, donc on évite les y=m_sizeHeight-1
							if( y==0 || (!isTheWall(x,y+1) && !hasMoreThanOne(x,y+1,m_labyCase[y][x])) ){
								m_labyCase[y+1][x] = m_labyCase[y][x];
								needRedraw = true;
							}
							break;

						case 3:// LEFT
							// On ne sort pas du tableau
							if( x <= 0 )
								break;

							// Fagocitage
							if( m_labyCase[y][x-1] != m_labyCase[y][x] && m_labyCase[y][x-1] != Simulation.WALL_ID ){
								replace(m_labyCase[y][x-1], m_labyCase[y][x]);
								nbKingdom--;
								break;
							}

							// Avec x=1, on ne crée pas de nouvelle brech
							// Ouverture de mur
							if( x == m_sizeWidth-1 || (!isTheWall(x-1,y) && !hasMoreThanOne(x-1,y,m_labyCase[y][x])) ){
								m_labyCase[y][x-1] = m_labyCase[y][x];
								needRedraw = true;
							}
							break;
					}
					if( viewStep && needRedraw ){
		    			if( Simulation.isInPause() ){
			    			Simulation.waitSimulation();
		    			}else{
		    				Thread.sleep(Simulation.speed);//sleep for {milli} ms
		    			}
						this.draw();
						needRedraw = false;
					}
				}
			}
		}while( nbKingdom > 1 );// Tant qu'il y a plusieurs groupe, on se bat

		// On reset les cases à vide
		for( int i=0, j=0; i<m_sizeHeight; i++ )
			for( j=0; j<m_sizeWidth; j++ )
				if( m_labyCase[i][j] != Simulation.WALL_ID )
					m_labyCase[i][j] = 0;

		// Fin de génération, on l'affiche tel quel
		this.draw();
	}


	/***********************************************************************//*!
	 * Parcours le laby pour remplacer toutes les valeurs des case = à {search}
	 * et les remplace par {repl}
	 * @param[in] search	Valeur recherchée
	 * @param[in] repl		Nouvelle valeur
	 * @return[NONE]
	 */
	private void replace( int search, int repl )
	{
		for( int x=0, y=0; y<m_sizeHeight; y++ )
			for( x=0; x<m_sizeWidth; x++ )
				if( m_labyCase[y][x] == search )
					m_labyCase[y][x] = repl;
	}


	/***********************************************************************//*!
	 * Permet de déterminer si le mur en x,y est un mur essentiel au laby
	 * @param[in] x		Position X
	 * @param[in] y		Position Y
	 * @return	TRUE si c'est un mur vitale. FALSE si non
	 */
	private boolean isTheWall( int x, int y )
	{
		return x==0 || y == 0 || x == m_sizeWidth-1 || y == m_sizeHeight-1;
	}


	/***********************************************************************//*!
	 * Permet de détecter si la case en x,y est entouré par + de 2 cases à valeur
	 * = à {search}.
	 * @param[in] x			Position X
	 * @param[in] y			Position Y
	 * @param[in] search	Valeur a chercher
	 * @return TRUE si 2 cases (ou +), correspondent à {search}
	 */
	private boolean hasMoreThanOne( int x, int y, int search )
	{
		int ret = 0;

		if( x+1 < m_sizeWidth && m_labyCase[y][x+1] == search )
			ret++;

		if( x >= 1 && m_labyCase[y][x-1] == search )
			ret++;

		if( y >= 1 && m_labyCase[y-1][x] == search )
			ret++;

		if( y+1 < m_sizeHeight && m_labyCase[y+1][x] == search )
			ret++;

		return ret>1;
	}


	/***********************************************************************//*!
	 * Configurer l'affichage des murs
	 * @param[in] showASCII		Afficher les murs en ASCII ?
	 * @return[NONE]
	 */
	public void setShowASCII( boolean showASCII )
	{
		m_showASCII = showASCII;
	}


	/***********************************************************************//*!
	 * Affichage des murs
	 * @return Afficher les murs en ASCII ?
	 */
	public boolean showASCII()
	{
		return m_showASCII;
	}


	/***********************************************************************//*!
	 * Configurer l'affichage des données contenue dans les cases
	 * @param[in] showCaseNumber	Afficher les données des cases ?
	 * @return[NONE]
	 */
	public void setShowCaseNumber( boolean showCaseNumber )
	{
		m_showCaseNumber = showCaseNumber;
	}


	/***********************************************************************//*!
	 * Permet d'obtenir la largeur du Laby
	 * @return Un nombre > 0
	 */
	public int width()
	{
		return m_sizeWidth;
	}


	/***********************************************************************//*!
	 * Permet d'obtenir la hauteur du Laby
	 * @return Un nombre > 0
	 */
	public int height()
	{
		return m_sizeHeight;
	}


	/***********************************************************************//*!
	 * Permet d'obtenir les données stockées dans la case en (X,Y)
	 * @param[in] x		Position x
	 * @param[in] y		Position y
	 * @return Les données
	 * @throws Exception	Renvoie une Exception si x ou y dépasse les limites du Laby
	 */
	public int getBlockInfo( int x, int y ) throws Exception
	{
		if( 0 > x || x >= m_sizeWidth || 0 > y || y >= m_sizeHeight )
			throw new Exception("getBlockInfo("+x+","+y+") MAX:("+(m_sizeWidth-1)+","+(m_sizeHeight-1)+") MIN:(0,0)");
		return m_labyCase[y][x];
	}


	/***********************************************************************//*!
	 * Permet de définir les données a stocker dans la case en (X,Y)
	 * @param[in] x		Position x
	 * @param[in] y		Position y
	 * @param[in] data	Les données a mettre. ( Phéromone + durée )
	 * @return[NONE]
	 * @throws Exception	Renvoie une Exception si x ou y dépasse les limites du Laby
	 */
	public void setBlockInfo( int x, int y, int data ) throws Exception
	{
		if( 0 > x || x >= m_sizeWidth || 0 > y || y >= m_sizeHeight )
			throw new Exception("setBlockInfo("+x+","+y+") MAX:("+(m_sizeWidth-1)+","+(m_sizeHeight-1)+") MIN:(0,0)");
		m_labyCase[y][x] = data;
	}


	/***********************************************************************//*!
	 * Permet de savoir s'il y a un mur "Droit" en (x,y)
	 * @param[in] x		Position x
	 * @param[in] y		Position y
	 * @return TRUE s'il y a un mur, FALSE sinon
	 * @note En cas de dépassement de capacité => pas de mur
	 */
	public boolean isRightWall( int x, int y ) throws Exception
	{

		if( 0 > x || x >= m_sizeWidth || 0 > y || y >= m_sizeHeight )
			return false;

		if( m_labyCase[y][x] == -1 )
			return true;

		if( x+1 >= m_sizeWidth )
			return false;

		return m_labyCase[y][x+1] == -1;
	}


	/***********************************************************************//*!
	 * Permet de savoir s'il y a un mur "Gauche" en (x,y)
	 * @param[in] x		Position x
	 * @param[in] y		Position y
	 * @return TRUE s'il y a un mur, FALSE sinon
	 * @note En cas de dépassement de capacité => pas de mur
	 */
	public boolean isLeftWall( int x, int y )
	{
		if( 0 > x || x >= m_sizeWidth || 0 > y || y >= m_sizeHeight )
			return false;

		if( m_labyCase[y][x] == -1 )
			return true;

		if( x-1 < 0 )
			return false;

		return m_labyCase[y][x-1] == -1;
	}


	/***********************************************************************//*!
	 * Permet de savoir s'il y a un mur "Bas" en (x,y)
	 * @param[in] x		Position x
	 * @param[in] y		Position y
	 * @return TRUE s'il y a un mur, FALSE sinon
	 * @note En cas de dépassement de capacité => pas de mur
	 */
	public boolean isBottomWall( int x, int y )
	{
		if( 0 > x || x >= m_sizeWidth || 0 > y || y >= m_sizeHeight )
			return false;

		if( m_labyCase[y][x] == -1 )
			return true;

		if( y+1 >= m_sizeHeight )
			return false;

		return m_labyCase[y+1][x] == -1;
	}


	/***********************************************************************//*!
	 * Permet de savoir s'il y a un mur "Haut" en (x,y)
	 * @param[in] x		Position x
	 * @param[in] y		Position y
	 * @return TRUE s'il y a un mur, FALSE sinon
	 * @note En cas de dépassement de capacité => pas de mur
	 */
	public boolean isTopWall( int x, int y ) throws Exception
	{
		if( y < 0 || y >= m_sizeHeight || x < 0 || x >= m_sizeWidth )
			return false;

		if( m_labyCase[y][x] == -1 )
			return true;

		if( y-1 < 0 )
			return false;

		return m_labyCase[y-1][x] == -1;
	}


	/***********************************************************************//*!
	 * Permet d'obtenir le nombre de case disposant du même chiffre
	 * @param[in] num	Le nombre a chercher
	 * @return Le nombre de case disposant du même chiffre
	 */
	public int getNbSameCaseNumber( int num )
	{
		int nb = 0;

		for( int i=0, j=0; i<m_sizeHeight; i++ )
			for( j=0; j<m_sizeWidth; j++ )
				if( m_labyCase[i][j] == num )
					nb++;
		return nb;
	}


	/***********************************************************************//*!
	 * Dessiner sans mettre de case en évidence
	 * @return[NONE]
	 */
	public void draw()
	{
		this.draw(Simulation.WALL_ID);
	}


	/***********************************************************************//*!
	 * Permet de dessiner le laby
	 * @param[in] HighlightCase		Mettre en évidence la case {@code HighlightCase}
	 * @return[NONE]
	 */
	public void draw( int HighlightCase )
	{
		// Si on a désactivé l'affichage, alors on passe
		if( m_doNotDraw )
			return ;

		// Si on n'est pas en mode console, on appel la fonction qui correspond.
		if( !Main_EntryPoint.isConsoleMode ){
			drawGraphics(HighlightCase);
			return ;
		}

		// Nettoyage de l'écran
		Console.cleanScreen();
		try{
			for( int y=0, x=0; y<m_sizeHeight; y++ )
			{
				for( x=0; x<m_sizeWidth; x++ )
				{
					if( m_labyCase[y][x] == Simulation.WALL_ID )
						Console.make(Console.BG_White);

					if( m_showASCII == true )
						Console.make(Console.Underline);

					if( m_labyCase[y][x] <= 0 || m_showCaseNumber == false ){
						System.out.print("     ");
					}else{
						System.out.printf(" %0"+m_caseSize+"d ", BitManager.getNumber(m_labyCase[y][x]));
					}

					if( m_showASCII == true ){
						System.out.print("|");
					}else{
						System.out.print(" ");
					}

					Console.cleanFont();
				}
				System.out.print("\n");
			}
			System.out.println("nb Output="+m_output.size()+" :: "+m_output.toString());
		}catch( Exception e ){
			Main_EntryPoint.file_put_contents(e);
		}
	}


	/***********************************************************************//*!
	 * Dessiner dans une ardoise GUI.
	 * @param[in] HighlightCase		Mettre en évidence la case {@code HighlightCase}
	 * @return[NONE]
	 */
	private void drawGraphics( int HighlightCase )
	{
		try {
			Board.cleanScreen();


			for( int x=0, y=0; y<m_sizeHeight; y++ )
			{
				for( x=0; x<m_sizeWidth; x++ )
				{
					if( m_labyCase[y][x] == Simulation.WALL_ID ){
						Board.setHorizontalColor(Color.BLACK);
						Board.drawRectangle(x, y);
						Board.cleanFont();
					}else if( m_labyCase[y][x] > 0 && m_showCaseNumber == true ){
						Board.drawString(String.format("%0"+m_caseSize+"d", m_labyCase[y][x]), x, y);
					}
				}
			}
		}catch( Exception e ){
			Main_EntryPoint.file_put_contents(e);
		}
	}


	/***********************************************************************//*!
	 * Permet d'obtenir les sorties du laby
	 * @return Liste des sorties du laby
	 */
	public final ArrayList<Pos> getOutput()
	{
		return m_output;
	}


	/***********************************************************************//*!
	 * Permet de tester cette classe, et voir si l'on ne perd pas des données en
	 * cours de route.
	 * @return[NONE]
	 * @throws Exception	Renvoie une Exception si la class ne passe pas tout les tests
	 */
	public static void tests() throws Exception
	{
		m_doNotDraw = true;
		Laby l = new Laby(20,20,new Pos(5,5));// Test d'allocation
		l.getBlockInfo(0, 0);// Test de récupération d'information
		l.getBlockInfo(19, 19);// Test de récupération d'information
		l.setBlockInfo(1, 1, 42);// Test d'affectation
		if( l.getBlockInfo(1, 1) != 42 )
			throw new Exception("Erreur lors de l'affectation ! La case en (1,1) devrait contenir 42 or elle contient: "+l.getBlockInfo(1, 1));

		l.setBlockInfo(1, 1, 1|((5 << 1) << 1));// Test d'affectation
		if( ((l.getBlockInfo(1, 1) >>> 1) >>> 1) != 5 )
			throw new Exception("Erreur lors de calcule ! La case en (1,1) devrait avoir une phéro de durée 5 or elle contient: "+((l.getBlockInfo(1, 1) >>> 1) >>> 1));

		m_doNotDraw = false;
	}
}
