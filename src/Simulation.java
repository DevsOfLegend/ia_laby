import java.io.FileInputStream;
import java.util.Scanner;

/***************************************************************************//*!
* C'est dans cette classe que la simulation est traitée.
* On peut changer les paramètres suivant:
*	- viewStep
*	- speed
*	- width
*	- height
*	- filename (fichier a charger)
*	- showCaseNumber
*
* Les paramètres suivant seront pris en charge même lorsque la simulation sera en cours:
*	- viewStep
*	- speed
*	- showCaseNumber
*
* Les variables ne sont pas protégées via des getter et setter car ces variables
* sont passées aux fonctions respectives. Les fonctions respectives renvoient des exceptions en cas de problème.
*
* Cette class est un thread, ceci dans le but d'éviter un freeze de l'interface graphique.
*/
public class Simulation extends Thread
{
	public static boolean viewStep = false;//!< Afficher l'animation de génération du Laby ?
	public static int speed = 150;//!< Vitesse de déplacement d'animation ( plus le chiffre est petit, plus l'animation est rapide ) ( le chiffre correspond au temps d'attente entre chaque tour )
	public static int width = 20;//!< Taille du Laby. (Attention si la valeur est incorrect une exception sera renvoyée)
	public static int height = 20;//!< Taille du Laby. (Attention si la valeur est incorrect une exception sera renvoyée)
	public static String filename = null;//!< fichier chargé en mémoire. ( N'est pris en compte qu'au boot )
	public static Behavior.type behavior = Behavior.type.BestFirst;//!< Algo de recherche par défaut

	private static Simulation m_inst = null;//!< Instance du Thread
	private static boolean m_putInPause = false;//!< Mettre en pause le thread ?
	private static boolean m_resetThread = false;//!< Mettre a zero le thread
	private static Laby m_laby = null;//!< Laby de la simulation
	private static Ants m_ants = null;//!< La fourmis qui va se déplacer
	private static boolean m_showCaseNumber = false;//!< Afficher les cases numérique du tableau

	private static Pos m_originalPos = null;//!< Position originel de la fourmi
	public final static int WALL_ID = -1;//!< ID des murs

	public static enum Status {
		Reseted,//!< La simulation est à 0. ( Permet de remettre à 0 la simulation )
		Started,//!< La simulation est en cours
		Stopped//!< Simulation en pause
	};
	public static Status m_currentStatus = Status.Reseted;//!< Status de la simulation.


	/***********************************************************************//*!
	 * Constructeur.
	 * Le constructeur est privé pour empêcher toute tentative de créer une
	 * instance de Simulation.
	 * La seule instance de Simulation est dans {m_inst}
	 * @return[NONE]
	 */
	private Simulation()
	{
		m_inst = this;
	}


	/***********************************************************************//*!
	 * Point d'entrée du Thread.
	 * Execution de la simulation ici !
	 * @return[NONE]
	 */
	@Override
	public void run()
	{
		Behavior.Algo_status isEnd = Behavior.Algo_status.FAIL;
		do{
			try{
				m_resetThread = false;
				m_currentStatus = Status.Started;

				/***************************************************************
				 * Lancement du laby
				 */
				if( Simulation.filename != null ){
					loadLaby();
				}else{
					// Position random de la fourmi
					m_originalPos = new Pos(Main_EntryPoint.rand(2, width-2), Main_EntryPoint.rand(2, height-2));

					m_laby = new Laby(width,height,(viewStep?1+(m_showCaseNumber?1:0):0),null,m_originalPos);
					m_laby.setShowASCII(true);
					m_laby.setShowCaseNumber(m_showCaseNumber);
				}
				m_ants = new Ants(behavior, m_originalPos.x(), m_originalPos.y());

				m_laby.draw(WALL_ID);
				m_ants.draw(m_laby);
				if( Main_EntryPoint.isConsoleMode ){
					Console.cleanFont();
					Console.gotoxy(0, height+2);
				}

				do{
					isEnd = m_ants.move(m_laby);
					m_laby.draw(WALL_ID);
					m_ants.draw(m_laby);

					if( Main_EntryPoint.isConsoleMode ){
						Console.cleanFont();
						Console.gotoxy(0, height+2);
					}

					if( speed != 0 && isEnd == Behavior.Algo_status.CONTINUE )
						sleep(speed);

					if( m_putInPause && isEnd == Behavior.Algo_status.CONTINUE )
						pauseIt();

				}while( isEnd == Behavior.Algo_status.CONTINUE || m_resetThread == true );

				// Message de fin
				if( isEnd != Behavior.Algo_status.CONTINUE && Main_EntryPoint.isConsoleMode == true ){
					System.out.println("Fin: "+isEnd.toString());
					System.out.println("Depart: "+m_originalPos.toString());
					System.out.println("Fourmi: ("+m_ants.getX()+";"+m_ants.getY()+")");
					System.out.println("Sortie: "+m_laby.getOutput().get(0).toString());
				}

				// On indique a l'interface que la simulation est terminée
				if( isEnd != Behavior.Algo_status.CONTINUE && Main_EntryPoint.isConsoleMode == false )
					GraphicalView.simulation_end_event();

			}catch( InterruptedException e ){
				System.out.println("Reset du thread - Fin du thread");
			}catch( java.lang.ClassCastException e ){// Peut arriver dans certains cas lors de l'interuption du thread (reset)
				//Main_EntryPoint.file_put_contents(e);
			}catch( Exception e ){
				Main_EntryPoint.file_put_contents(e);
			}

			try{
				// On indique a l'interface que la simulation est terminée
				if( Main_EntryPoint.isConsoleMode == false ){
					GraphicalView.simulation_end_event();
					// On met en pause après un reset
					pauseIt();
				}
			}catch( InterruptedException e ){}// On sait ce que l'on fait
		}while( m_resetThread == false && Main_EntryPoint.isConsoleMode == false );
	}


	/***********************************************************************//*!
	 * Permet de remettre à zéro la simulation
	 * @return[NONE]
	 */
	public static void reset()
	{
		if( m_inst == null ){
			new Simulation();
			return ;
		}
		pause();// On met d'abord en pause sans quoi le interrupt n'est pas pris en compte
		m_currentStatus = Status.Reseted;
		m_resetThread = true;
		// On saute direct dans les catch ( InterruptedException de run() ).
		// On est obligé dans certain cas d'interompre totalement le thread.
		m_inst.interrupt();
	}


	/***********************************************************************//*!
	 * Permet de lancer/de reprendre la simulation.
	 * @return[NONE]
	 */
	public static void play()
	{
		m_currentStatus = Status.Started;
		m_putInPause = false;
		if( m_inst == null ){
			new Simulation();
			m_inst.start();// Lancement du thread
		}else{
			try{
				m_inst.resumeIt();
			}catch( IllegalMonitorStateException e ){
				Main_EntryPoint.file_put_contents(e);
			}
		}
	}


	/***********************************************************************//*!
	 * Permet de reprendre la simulation.
	 * Envoie un signal au thread pour lui ordonner de se réveiller.
	 * @return[NONE]
	 */
	private synchronized void resumeIt()
	{
		m_currentStatus = Status.Started;
		m_resetThread = false;
		m_putInPause = false;
		notifyAll();// Wake up thread ! It's time to work :D
		notify();
	}


	/***********************************************************************//*!
	 * Permet de mettre en pause la simulation
	 * @return[NONE]
	 * @note Cette fonction appellera indirectement pauseIt()
	 */
	public static void pause()
	{
		m_putInPause = true;
	}


	/***********************************************************************//*!
	 * Permet de savoir si la simulation est en pause
	 * @return FALSE si la simulation est en cours. TRUE dans tout les autres cas ( pause, reset )
	 */
	public static boolean isInPause()
	{
		return m_putInPause;
	}


	/***********************************************************************//*!
	 * Force le thread de simulation a se mettre en pause.
	 * @return[NONE]
	 * @throws InterruptedException Si on cherche a reset le thread
	 */
	private synchronized void pauseIt() throws InterruptedException
	{
		if( m_resetThread == true ){
			m_currentStatus = Status.Reseted;
		}else{
			m_currentStatus = Status.Stopped;
		}
		this.wait();
	}


	/***********************************************************************//*!
	 * Met en pause en thread courant et attend que la simulation reprenne.
	 * @return[NONE]
	 * @throws InterruptedException  Si on cherche a reset le thread
	 * @note Si aucune instance de Simulation n'est trouvée alors une nouvelle
	 * instance est crée. De plus aucun blockage ne sera effectué.
	 */
	public static void waitSimulation() throws InterruptedException
	{
		if( m_inst == null ){
			new Simulation();
			return ;
		}
		m_inst.pauseIt();
	}


	/***********************************************************************//*!
	 * Permet de connaître le status de la simulation.
	 * @return Le status de la simulation.
	 */
	public static Status currentStatus()
	{
		return m_currentStatus;
	}


	/***********************************************************************//*!
	 * Permet de dessiner la simulation en cours.
	 * @return[NONE]
	 * @note Cette fonction est utilisé pour la version graphique lors des redimensionnement.
	 */
	public static void draw()
	{
		if( m_laby != null )
			return ;
		m_laby.draw(WALL_ID);
		m_ants.draw(m_laby);
	}


	/***********************************************************************//*!
	 * Permet de charger un laby de puis un fichier (Simulation.filename)
	 * @return[NONE]
	 */
	private static void loadLaby()
	{
		// Lecture du fichier texte
		try{
			FileInputStream fis=new FileInputStream(Simulation.filename);
			Scanner scan = new Scanner(fis);

			// On charge la position de la fourmi
			m_originalPos = new Pos();
			m_originalPos.setX(scan.nextInt());
			m_originalPos.setY(scan.nextInt());
			Simulation.width = scan.nextInt();
			Simulation.height = scan.nextInt();
			m_laby = new Laby(scan, Simulation.width, Simulation.height);
			m_laby.setShowASCII(false);
			m_laby.setShowCaseNumber(m_showCaseNumber);
			scan.close();
		}catch( Exception e ){
			Main_EntryPoint.file_put_contents(e);
		}
	}


	/***********************************************************************//*!
	 * Passerelle vers laby
	 * @param scn	cf Laby
	 * @return[NONE]
	 */
	public static void setShowCaseNumber( boolean scn )
	{
		m_showCaseNumber = scn;
		if( m_laby != null )
			m_laby.setShowCaseNumber(scn);
	}


	/***********************************************************************//*!
	 * Passerelle vers laby
	 * @return cf Laby
	 */
	public static boolean showCaseNumber()
	{
		return m_showCaseNumber;
	}
}
